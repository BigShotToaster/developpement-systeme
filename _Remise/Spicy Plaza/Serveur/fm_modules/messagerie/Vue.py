## -*- coding: UTF-8 -*- 

from tkinter import *
from SETTINGS import *
import re
from tkinter.ttk import Treeview
from datetime import date, time, datetime
from PIL import Image

import random

class Vue():
    def __init__(self,parent):
        self.parent=parent
        self.root=Tk()
        self.root.title('Spicy Plaza - La Messagerie')
        self.root.configure(background=COLOR_WHITE)
        
        self.root.geometry(str(SCREEN_DIM_X)+"x"+str(SCREEN_DIM_Y))
        self.scrollY = 0
        self.printY = 0
        
        # Déclaration de tous les Frames principaux
        self.topPanel = Frame()
        self.centerCanvevas = Canvas()
        self.convosPanel = Frame()
        self.yscrollbar = Scrollbar()
        self.centerFrame = Frame()
        self.searchPanel = Frame()
                
        self._convos = {}
        self.photosAvatar = {}
        
        self.root.bind("<MouseWheel>", self.OnMouseWheel)
        
        
    def iniModule(self,conversations,users,userID):
        # Destruction de tous les frames principaux, aux cas ou un appel serait refait vers iniModule
        self.centerCanvevas.destroy()
        self.topPanel.destroy()
        self.convosPanel.destroy()
        self.yscrollbar.destroy()
        self.centerFrame.destroy()
        
        self.topPanel=Frame(self.root,width=SCREEN_DIM_X,height=50, bg = COLOR_GREEN)      
        self.topPanel.pack(fill = BOTH, side = TOP)
        
        self.img = PhotoImage(file="./modules/messagerie/img/SpicyPlaza_line.png")  
        label = Label(self.topPanel,image=self.img,bg=COLOR_GREEN)
        label.pack(side=LEFT,padx=60)
        
        self.img_refresh = PhotoImage(file="./modules/messagerie/img/refresh.png")
        self.refreshBtn = Button(self.topPanel,cursor="hand2",image=self.img_refresh, font = "GENERALFONT 12 bold underline",relief=FLAT,bd = 0, bg=COLOR_GREEN,fg="#ffffff",activebackground=COLOR_GREEN,activeforeground="#e8e8e8", command=self.refreshView)
        self.refreshBtn.pack(padx = 10,side=RIGHT,pady=10)
        
        self.messagerieBtn = Button(self.topPanel,cursor="hand2",text="Vitrine", font = "GENERALFONT 12 bold underline",relief=FLAT,bd = 0, bg=COLOR_GREEN,fg="#ffffff",activebackground=COLOR_GREEN,activeforeground="#e8e8e8", command=self.parent.ouvertureModuleVitrine)
        self.messagerieBtn.pack(padx = 10,side=RIGHT,pady=10)
        
        self.convosPanel=Frame(self.root,width=SCREEN_DIM_X/6,height=SCREEN_DIM_Y,  bg=COLOR_WHITE)      
        self.convosPanel.pack(side = LEFT,fill=BOTH)
        
        self.searchPanel=Frame(self.convosPanel,bg=COLOR_WHITE,height=50,bd=0)
        self.searchPanel.pack(side=TOP,fill=X,pady=5)
        
        self.varEmail = StringVar(self.root)
        self.varEmail.set("Name")
        
        
        self.userSearch = Entry(self.searchPanel,textvariable=self.varEmail)
        self.userSearch.configure(font = "GENERALFONT 10 bold",relief=FLAT,bd = 0, fg=COLOR_TXT,bg="#ffffff",width=25)
        self.userSearch.pack(fill=BOTH,side=LEFT,expand=TRUE,padx=5)
        
        self.img_search = PhotoImage(file="./modules/messagerie/img/loupe.png")
        self.searchBtn = Button(self.searchPanel,cursor="hand2",font = "GENERALFONT 12 bold",fg="white",activeforeground=COLOR_TXTACTIVE,activebackground=COLOR_LIGHTGREEN,height=50,width=50,image=self.img_search,relief=FLAT,bg=COLOR_GREEN,bd=0,command=self.searchUser)
        self.searchBtn.pack(side=RIGHT,padx=5,fill=BOTH)
        
        # Création de la scrollbar à droite affectant le canevas
        self.yscrollbar = Scrollbar(self.root)
        self.yscrollbar.pack(side=RIGHT,fill=Y)
        
        self.centerFrame=Frame(self.root,bg=COLOR_WHITE)
        self.centerFrame.pack(side = TOP, fill = BOTH)
        
        self.usernameFrame = Frame(self.centerFrame,bg="white",height=50)
        self.usernameFrame.pack(side=TOP,fill = BOTH,expand=TRUE,padx=20,pady=5)
        
        self.usernameTxt = Label(self.usernameFrame,text=" ",font="GENERALFONT 14 bold",fg=COLOR_TXTACTIVE,bg="white")
        self.usernameTxt.pack(fill=BOTH,expand=TRUE,pady=10)
       
        self.inputFrame = Frame(self.centerFrame)
        self.inputFrame.pack(side=BOTTOM,fill = BOTH,padx=15,pady=5,expand=TRUE)
       
        # Création du canevas centrale
        # yscrollcommand sert à affecter la scrollbar créée plus haut
        # scrollregion permet de définir une région limitée (puisqu'un canevas est normalement illimité en dimension). Cela permet de setter la hauteur maximale du scroll. Pour l'instant, self.scrollY égal 0
        self.centerCanvevas=Canvas(self.centerFrame,height=SCREEN_DIM_Y-20,bg=COLOR_WHITE,bd=0,highlightthickness=0,yscrollcommand=self.yscrollbar.set,scrollregion=(0, 0, 0, 0))       
        self.centerCanvevas.pack(side = TOP, padx=20,fill = BOTH)
        
        self.centerCanvevas.yview('scroll', 1, 'units')
        self.centerCanvevas.update()
        canvasHeight = self.centerCanvevas.winfo_height()
        self.centerCanvevas.config(scrollregion=(0, 0, 0, canvasHeight))
       
        self.inputBtn = Button(self.inputFrame,cursor="hand2",font = "GENERALFONT 12 bold",fg="white",activeforeground=COLOR_TXTACTIVE,activebackground=COLOR_LIGHTGREEN,width=10,text="Envoyer",relief=FLAT,bg=COLOR_GREEN,bd=0, command = self.sendMessage)
        self.inputBtn.pack(side=RIGHT,padx=5,fill=Y)
      
        self.inputTxt = Text(self.inputFrame,padx=10,pady=10,height=5,bd=0,font = "GENERALFONT 10")
        self.inputTxt.pack(fill=BOTH,padx=5,expand=TRUE,side=LEFT)
        
        
        # Bind de la commande de la scrollbar à la vue Y du canevas
        self.yscrollbar.config(command=self.centerCanvevas.yview)
        
        
        # Recentrage de la fenêtre, à appeler après l'avoir rempli
        self.center(self.root)
        
        self.convoInsideFrame = Frame()
        
        if userID != -1 and conversations != -1:
            self.updateCanevas(conversations[userID],users[userID])
            self.printConvo(conversations,users)
        
     
    def sendMessage(self):
        txt = self.inputTxt.get(1.0, 'end-1c')
        if txt != "":      
        
            #Supprimer le contenu du Text widget après avoir l'avoir récupéré
            self.inputTxt.delete('1.0', END)
            self.inputTxt.update()      
            receiverID = self.parent.actualConvo
            myID = self.parent.MyID
    
            self.parent.addMessage(myID, receiverID, txt)
            self.parent.refreshView()
            #self.parent.changeConvo(receiverID)

    def refreshView(self):
        self.inputTxt.delete(1.0, END)
        self.userSearch.delete(0, 'end')
        self.parent.refreshView()
        self.varEmail.set("Name")
       
    def updateCanevas(self,convo,user,resetPosition=1):
        self.printY = 0
        self.scrollY = 0
        self.centerCanvevas.delete("post")
        
        self.centerCanvevas.update()
        canvasHeight = self.centerCanvevas.winfo_height()
        
        if(convo or user) != None:
            # Afficher toutes les publications
            for message in convo.messages:
                self.printMessage(message)
                # Si resetPosition est activé, ajouter 10 à la scrollRegion pour chaque publication
                if(resetPosition):
                    self.centerCanvevas.config(scrollregion=(0, -self.scrollY+canvasHeight, 0, canvasHeight))
                  
            self.centerCanvevas.config(scrollregion=(0, -self.scrollY+canvasHeight, 0, canvasHeight))
            if(resetPosition): 
                self.centerCanvevas.yview('scroll', 1, 'units')
                self.centerCanvevas.update()
                
            self.usernameTxt.config(text=user.prenom+" "+user.nom)
        
    def printConvo(self,conversations,users):
        self.convoInsideFrame.destroy()
        self.convoInsideFrame=Frame(self.convosPanel,bg = COLOR_WHITE)
        self.convoInsideFrame.pack(padx=5,fill=BOTH)
        if users != None and conversations != None:
            for userId in conversations:
                if userId == self.parent.actualConvo:
                    color = 'white'
                    bg = COLOR_GREEN
                else:
                    color = COLOR_TXTACTIVE
                    bg = 'white'
                    
                # Image Avatar
                user = users[userId]
                
                if user.localPath != None:
                    path = user.localPath
                else:
                    path = "./modules/messagerie/img/profil_blue_small.png"
                
                im_name = user.prenom+user.nom+str(user.id)+".ppm"
                
                im_temp = Image.open(path)
                
                im_temp = im_temp.resize((30, 30), Image.ANTIALIAS)
                im_temp.save("./tmp/small/"+im_name, "ppm")
                self.photosAvatar[user.id] = PhotoImage(file="./tmp/small/"+im_name) 
                
                
                username = Button(self.convoInsideFrame,image=self.photosAvatar[user.id],compound='left',padx=10,command=lambda:self.changeConvo(user.id),text=user.prenom+" "+user.nom,bg=bg,font="GENERALFONT 10 bold",fg=color,anchor=W,height=40,relief=FLAT,bd=0,activeforeground=COLOR_BLACK,activebackground=COLOR_LIGHRED,cursor="hand2")
                username.config(command=lambda j=user.id:self.changeConvo(j))
                username.pack(fill=X,pady=5)
    
    def changeConvo(self,userID):
        self.parent.changeConvo(userID)
        
    def changeNewConvo(self,userID):
        self.parent.changeNewConvo(userID)
    
    def printMessage(self,message):
        # Déclaration des valeurs de la publication et du User
        messageTxt = message.message
        date = message.date
        mine = message.mine
        
        # Setting de la date
        strDate=""
        maintenant = datetime.now()
        
        # Calcul pour soit afficher le nombre de minute si cela fait moins d'une heure, soit afficher la date complète
        if date.date() == maintenant.date() and maintenant.hour - date.hour < 1 :
            if maintenant.minute - date.minute > 0:
                strDate += str(maintenant.minute-date.minute)+"min"
            else:
                strDate += "<1min"
        else:
            if date.date() != maintenant.date():
                strDate += str(date.year)+"-"+str(date.month)+"-"+str(date.day)+" | "
            
            strDate += str(date.hour)+"h "
                
            if date.minute > 9:
                strDate += str(date.minute)
            else:
                strDate += "0"+str(date.minute)
        
        # Setting de la page
        self.centerCanvevas.update()
        canvasWidth = self.centerCanvevas.winfo_width()
        canvasHeight = self.centerCanvevas.winfo_height()
        
        color="#ffffff"
        left = 30
        top = 0    # Calculé selon la position Y d'impression
        right = int(canvasWidth/2)+60 
        padding = 10
        
        if mine:
            color=COLOR_LIGHTGREEN
            left = int(canvasWidth/2)-60 
            right = canvasWidth-30
        
        width = right-padding*3-left
        font="GENERALFONT 8"
        
        # texte sur canevas
        # Création d'un objet text sur le canevas afin de récupérer la hauteur nécessaire pour le texte
        publication = self.centerCanvevas.create_text(left+padding*2,top+padding,text=messageTxt,anchor=NW,width=width,tags=("post","text",))
        
        # Récupération des bounds de la fenêtre et calcul de la hauteur de la publication
        bounds = self.centerCanvevas.bbox(publication)
        height = (bounds[3] - bounds[1])+15

        # Calcul du bottom de la publication selon le top et la hauteur
        bott = canvasHeight-self.printY-20
        top = bott-height
        
        # texte dans window sur canevas
        # Création d'un widget Text avec les même paramètre que l'objet Text du Canevas plus haut
        # Ceci permettra d'avoir un text sélectionnable
        text = Text(self.centerCanvevas,fg="grey25",bd=0,bg=color,font=font,selectbackground="#e57360")
        text.pack()
        text.insert(END,messageTxt)
        text.config(state=DISABLED)
        
        # Création d'un objet Window sur le Canevas permettant d'afficher des Widget Tkinter dans un espace géré par un canevas
        # Le scroll fonctionne pour ces objets aussi
        window = self.centerCanvevas.create_window(left+padding*2,top+padding,window=text,anchor=NW,width=width,height=height-15,tags=("post","window",))
        
        
        # Impression du cadre visuel de la publication
        # Rectangle de publication
        self.centerCanvevas.create_rectangle(left,top,right,bott,fill=color,width=0,tags=("post","bg",))
        
        # Coins arrondis
        self.centerCanvevas.create_oval(right-padding,top-padding,right+padding,top+padding,fill=color,width=0,tags=("post","coin",))
        self.centerCanvevas.create_oval(left-padding,top-padding,left+padding,top+padding,fill=color,width=0,tags=("post","coin",))
        self.centerCanvevas.create_oval(right-padding,bott-padding,right+padding,bott+padding,fill=color,width=0,tags=("post","coin",))
        self.centerCanvevas.create_oval(left-padding,bott-padding,left+padding,bott+padding,fill=color,width=0,tags=("post","coin",))
        
        # Bordure 
        self.centerCanvevas.create_rectangle(left,top-padding,right,top,fill=color,width=0,tags=("post","border",))
        self.centerCanvevas.create_rectangle(left,bott,right,bott+padding+1,fill=color,width=0,tags=("post","border",))
        self.centerCanvevas.create_rectangle(left-padding,top,left,bott,fill=color,width=0,tags=("post","border",))
        self.centerCanvevas.create_rectangle(right,top,right+padding+1,bott,fill=color,width=0,tags=("post","border",))
        
        
        # Date
        font="GENERALFONT 8"
        self.centerCanvevas.create_text(right-padding,top+padding/3,text=strDate,anchor=E, font = font, fill="#999999",tags=("post","date",))
        
        # Supression de l'objet Text sur Canevas; Inutile maintenant que nous avons les hauteurs nécessaires
        self.centerCanvevas.delete("text")
        
        # Incrémentation du scroll et de la position actuelle d'impression
        self.scrollY = self.scrollY+height+30
        self.printY = self.printY+height+30
        
    def OnMouseWheel(self,event):
        count = 0
        if event.num == 5 or event.delta == -120:
            count =1
        if event.num == 4 or event.delta == 120:
            count =-1
        self.centerCanvevas.yview('scroll', count, 'units')
        self.centerCanvevas.update()
   
    # Fonction pour centrer une fenêtre, à utiliser après qu'elle ait déjà été remplie
    def center(self,win):
        """
        centers a tkinter window
        :param win: the root or Toplevel window to center
        """
        win.update_idletasks()
        width = win.winfo_width()
        frm_width = win.winfo_rootx() - win.winfo_x()
        win_width = width + 2 * frm_width
        height = win.winfo_height()
        titlebar_height = win.winfo_rooty() - win.winfo_y()
        win_height = height + titlebar_height + frm_width
        x = win.winfo_screenwidth() // 2 - win_width // 2
        y = win.winfo_screenheight() // 2 - win_height // 2
        win.geometry('{}x{}+{}+{}'.format(width, height, x, y))
        win.deiconify()
        
    def searchUser(self):
        userName = self.userSearch.get()
        self.parent.filterUserConvo(userName)

    def updateUserAfficheAGauche(self, conversations,users, otherUsers = None):
        self.convoInsideFrame.destroy()
        self.convoInsideFrame=Frame(self.convosPanel,bg = COLOR_WHITE)
        self.convoInsideFrame.pack(padx=5,fill=BOTH)
        
        if users != None and conversations != None:
            for userId in conversations:
                if userId == self.parent.actualConvo:
                    color = 'white'
                    bg = COLOR_GREEN
                else:
                    color = COLOR_TXTACTIVE
                    bg = 'white'
                    
                # Image Avatar
                user = users[userId]
                
                if user.localPath != None:
                    path = user.localPath
                else:
                    path = "./modules/messagerie/img/profil_blue_small.png"
                
                im_name = user.prenom+user.nom+str(user.id)+".ppm"
                
                im_temp = Image.open(path)
                
                im_temp = im_temp.resize((30, 30), Image.ANTIALIAS)
                im_temp.save("./tmp/small/"+im_name, "ppm")
                self.photosAvatar[user.id] = PhotoImage(file="./tmp/small/"+im_name) 
                
                username = Button(self.convoInsideFrame,image=self.photosAvatar[user.id],compound='left',padx=10,command=lambda:self.changeConvo(user.id),text=user.prenom+" "+user.nom,bg=bg,font="GENERALFONT 10 bold",fg=color,anchor=W,height=40,relief=FLAT,bd=0,activeforeground=COLOR_BLACK,activebackground=COLOR_LIGHRED,cursor="hand2")
                username.config(command=lambda j=user.id:self.changeConvo(j))
                username.pack(fill=X,pady=5)
            
        if otherUsers:
            for user in otherUsers.values():
                color = COLOR_TXTACTIVE
                bg = "#cecece"
                
                # Image Avatar
                im_name = user.prenom+user.nom+str(user.id)+".ppm"
                
                if user.localPath != None:
                    path = user.localPath
                else:
                    path = "./modules/messagerie/img/profil_blue_small.png"
                
                im_temp = Image.open(path)
                
                im_temp = im_temp.resize((30, 30), Image.ANTIALIAS)
                im_temp.save("./tmp/small/"+im_name, "ppm")
                self.photosAvatar[user.id] = PhotoImage(file="./tmp/small/"+im_name) 
                    
                username = Button(self.convoInsideFrame,image=self.photosAvatar[user.id],compound='left',padx=10,command=lambda:self.changeConvo(user.id),text=user.prenom+" "+user.nom,bg=bg,font="GENERALFONT 10 bold",fg=color,anchor=W,height=40,relief=FLAT,bd=0,activeforeground=COLOR_BLACK,activebackground=COLOR_LIGHRED,cursor="hand2")
                username.config(command=lambda j=user.id:self.changeNewConvo(j))
                username.pack(fill=X,pady=5)