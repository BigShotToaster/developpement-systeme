# -*- coding: utf-8 -*-
# Version 
# 1

import xmlrpc.client
import os,os.path
import sys
import socket
from subprocess import Popen 

from SETTINGS import *
from Vue import *
from Modele import * 


class Controleur():
    def __init__(self,id=1):
        self.modele= Modele(self) 
        self.vue= Vue(self)
        self.server=None
        self.MyID = int(sys.argv[1])
        self.allUserId=[]
    
        #*******************WHY?**********************#
        self.actualConvo = int(sys.argv[2])
        self.ip = sys.argv[3]
       
        
        self.loginclient(self.ip)
        
        # Ne faire la suite que si un server est connectÃ©
        if self.server:
            self.iniModule()
            self.vue.root.mainloop()

        
    def loginclient(self,ip="127.0.0.1",port="50017"):
        ad="http://"+ip+":"+port
        self.server=xmlrpc.client.ServerProxy(ad)
        print(self.server)    
        
    def downloadImage(self, path, id):
        if path !='0':
            rep=self.server.requetefichier(path)
            lieu = ".\\img"
            if not os.path.exists(lieu):
                    os.mkdir(lieu) 
            lieu = lieu + "\\" + str(id)
            if not os.path.exists(lieu):
                os.mkdir(lieu) 
            list = os.listdir(lieu) # dir is your directory path
            numberFiles = len(list)
            lieu = lieu+"\\"+ str(0)+ ".jpg"
            fiche=open(lieu,"wb")
            
            fiche.write(rep.data)
            fiche.close() 
            self.modele.users[id].setLocalPath(lieu)        

    def getUserConvos(self):
        usersID = self.server.getUsersMessage(self.MyID)
        for user in usersID:
            userID = -1
            if user[0] != self.MyID:
                userID = user[0]
            else:
                userID = user[1]
            
            user = self.server.getUser(userID)
            self.modele.addProfil(user)
            self.downloadImage(self.modele.users[user[0]].pathImage, user[0])
            self.modele.createEmptyConvo(userID)
        
         
    def getFirstConvo(self,userID):
        messagesID = self.server.getConvo(self.MyID,userID)
        if messagesID:
            if messagesID[0]:
                for messageID in messagesID:
                    message = self.server.getMessageByID(messageID[0])
                    
                    mine = False
                    if messageID[1] == self.MyID:
                        mine = True
                        
                    self.modele.addMessage(message, userID, mine)
            
    def getNextMessages(self):
        messagesID = self.server.getNextMessages(self.MyID,self.actualConvo,self.modele.conversations[self.actualConvo].messages[-1].id)
        if messagesID:
            for messageID in messagesID:
                message = self.server.getMessageByID(messageID[0])
                
                mine = False
                
                if messageID[1] == self.MyID:
                    mine = True
                    
                self.modele.addMessage(message, self.actualConvo, mine)
            return 1
        else:
            return 0
    
    def iniModule(self):
        self.getUserConvos()
        req = list(self.modele.users.keys())
        if req:
            if self.actualConvo == -1:
                self.actualConvo = req[0]
                self.getFirstConvo(self.actualConvo)
            else:
                if self.actualConvo in list(self.modele.conversations.keys()):
                    self.getFirstConvo(self.actualConvo)
                else:
                    self.newConvo()
        
            self.vue.iniModule(self.modele.conversations,self.modele.users,self.actualConvo)
            self.checkNew()    
        else:
            if self.actualConvo != -1:
                self.newConvo()
                self.vue.iniModule(self.modele.conversations,self.modele.users,self.actualConvo)
            else:
                self.vue.iniModule(-1,-1,-1)
        
    def newConvo(self):
        user = self.server.getUser(self.actualConvo)
        self.modele.addProfil(user)
        self.downloadImage(self.modele.users[user[0]].pathImage, user[0])
        
        self.modele.createEmptyConvo(self.actualConvo)
    
    def checkNew(self):
        if self.vue.centerCanvevas.yview()[0] < 0.1 and self.vue.centerCanvevas.yview()[1] < 0.9:
            
            new = self.getNextMessages()
            
            if new:
                self.vue.updateCanevas(self.modele.conversations[self.actualConvo],self.modele.users[self.actualConvo],0)
        elif self.vue.centerCanvevas.yview()[1] > 0.9:
            # Regarder si le dernier message local de cette convo est le dernier message server
            # Si non, refresh la page
            
            req = self.server.getLastMessageID(self.MyID,self.actualConvo)
            
            
            if req != -1:
                if self.modele.conversations[self.actualConvo].messages:
                    if req != self.modele.conversations[self.actualConvo].messages[0].id:
                        self.refreshView()
            
        self.vue.root.after(1000,self.checkNew)
        
    def changeConvo(self,userID):
        self.actualConvo = userID
        self.refreshView()
        
    def changeNewConvo(self,userID):
        self.actualConvo = userID
        self.newConvo()
        
        self.vue.updateCanevas(self.modele.conversations[self.actualConvo],self.modele.users[self.actualConvo])
        
    def refreshView(self,resetPosition=1):
        self.modele.clearConvos()
        self.getUserConvos()
        
        if self.actualConvo in list(self.modele.conversations.keys()):
            self.getFirstConvo(self.actualConvo)
            self.vue.updateCanevas(self.modele.conversations[self.actualConvo],self.modele.users[self.actualConvo],resetPosition)
        else:
            req = list(self.modele.conversations.keys())
            if req:
                self.actualConvo = req[0]
            self.getFirstConvo(self.actualConvo)
            
            self.vue.updateCanevas(self.modele.conversations[self.actualConvo],self.modele.users[self.actualConvo],resetPosition)
        self.vue.printConvo(self.modele.conversations, self.modele.users)
            
    def addMessage(self,idUser,idReceiver, message):
        self.server.addMessage(idUser,idReceiver, message)

    def filterUserConvo(self, userNameEntry):
        searchResult = []
        searchResultAll = []
        searchConvo = {}
        
        userNameEntry = userNameEntry.split();
        
        for userName in userNameEntry:
            for userID in self.modele.conversations:
                user = self.modele.users[userID]
                if user.nom == userName or user.prenom == userName:
                    searchResult.append(user.id)
                    searchConvo[user.id]=self.modele.conversations[user.id]
        
        for userName in userNameEntry:
            req = self.server.getUsersIDByName(userName)
            if req:
                for id in req:
                    if id[0] not in self.modele.conversations:
                        user = self.server.getUser(id[0])
                        self.modele.addProfil(user)
                        self.downloadImage(self.modele.users[user[0]].pathImage, user[0])
                        searchResultAll.append(id[0])
        
        usersToUpdate = {}
        usersAll = {}
        
        for resultId in searchResult:
            usersToUpdate[resultId] = self.modele.users[resultId]
        
        for resultId in searchResultAll:
            usersAll[resultId] = self.modele.users[resultId]

        if len(userNameEntry) == 0 and len(usersToUpdate.keys()) == 0:
            self.vue.updateUserAfficheAGauche(self.modele.conversations,self.modele.users)
        else:
            self.vue.updateUserAfficheAGauche(searchConvo,usersToUpdate, usersAll)

    def getRandomMessage(self):
        message = self.server.getRandomMessage
        #faire afficher message avec le message reçu
        
    def refreshMessages(self):
        print("refresh messages")
        self.refreshView()
        
    def ouvertureModuleVitrine(self, idContact = -1):
        repRequeteModule = self.server.requetemodule("vitrine")
        #print(repRequeteModule)
        if repRequeteModule:
            #print(repRequeteModule[0])
            cwd=os.getcwd()
            lieuApp="\\"+repRequeteModule[0]
            lieu=cwd+"\\modules"
            if not os.path.exists(lieu):
                os.mkdir(lieu) #plante s'il exist deja
            lieu=lieu+lieuApp
            #print(lieu)
            if not os.path.exists(lieu):
                os.mkdir(lieu) #plante s'il exist deja
            bonPath=repRequeteModule[1]
            #print(repRequeteModule[1])
            #print("contenu dossier/fichier", repRequeteModule[2])
            for i in repRequeteModule[2]:
                #print(i)
                if i[0]=="fichier":
                    nom=bonPath+i[1]
                    #print("fichier", nom)
                    rep=self.server.requetefichier(nom)
                    fiche=open(lieu+"\\"+i[1],"wb")
                    fiche.write(rep.data)
                    fiche.close()
                elif i[0]=="dossier":
                    if not os.path.exists(lieu+"\\"+i[1]):
                        os.mkdir(lieu+"\\"+i[1])
                elif i[0]=="sous-fichier":
                    nom=bonPath+i[1]+'\\'+i[2]
                    #print("sous-fichier", nom)
                    rep=self.server.requetefichier(nom)
                    fiche=open(lieu+"\\"+i[1]+"\\"+i[2],"wb")
                    fiche.write(rep.data)
                    fiche.close()
            chaineappli=lieu+lieuApp+".py"

            self.pid = Popen([sys.executable, chaineappli,str(self.MyID), self.ip, self.ip],shell=0)
        else:
            print("RIEN")

if __name__ == '__main__':
    c=Controleur()