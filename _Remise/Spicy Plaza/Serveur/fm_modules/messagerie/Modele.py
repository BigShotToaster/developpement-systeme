# -*- coding: utf-8 -*-
from SETTINGS import *
from threading import Timer

from User import *

import re
import time
import random
from Conversation import *
from Message import *


class Modele():
    def __init__(self,parent):
        self.parent=parent
        self.users = {}
        self.conversations = {}
    
    # Ajout d'un user, retour pour MonID
    def addProfil(self,user):
        myProfil = User(self,user[0],user[3],user[2],user[1],user[5])
        self.users[user[0]] = myProfil

    def createEmptyConvo(self,userID):
        if userID not in self.conversations:
            self.conversations[userID] = Conversation(userID)
            

    def addMessage(self,message,userID,mine):
        if userID not in self.conversations:
            self.conversations[userID] = Conversation(userID)
        
        self.conversations[userID].messages.append(Message(message[0],message[1],mine,message[2]))
   
    def clearConvos(self):
        self.conversations = {}

