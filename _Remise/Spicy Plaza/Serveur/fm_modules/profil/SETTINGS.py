## -*- coding: UTF-8 -*- 

#Constante Global
#----------------
#DISPLAY SETTINGS
SCREEN_DIM_X = 1280        #Largeur total de l'�cran
SCREEN_DIM_Y = 900          #Hauteur total de l'�cran
CANVAS_DIM_X = SCREEN_DIM_X/2.1          #Largeur de la zone de jeu
CANVAS_DIM_Y = SCREEN_DIM_Y*.8          #Hauteur de la zone de jeu

GENERALFONT = "helvetica"

COLOR_WHITE = "#efefea"
COLOR_RED = "#e05841"
COLOR_LIGHRED = "#e26a56"

COLOR_TXTACTIVE = "#6d6d6d"
COLOR_TXT = "#999999"

COLOR_BLACK = "grey25" #"#353330"
COLOR_GREEN = "#97cb52"
COLOR_LIGHTGREEN = "#bdef7a"
COLOR_YELLOW = "#f9b450"
COLOR_LIGHYELLOW = "#ffc168"

#GAME SETTINGS
VITESSE_REFRESH = 10

