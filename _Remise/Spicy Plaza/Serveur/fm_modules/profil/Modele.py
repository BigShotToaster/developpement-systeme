# -*- coding: utf-8 -*-
from SETTINGS import *
from threading import Timer

from Publication import *
from User import *

import re
import time
import random


class Modele():
    def __init__(self,parent):
        self.parent=parent
        self.users = {}
        self.interets = {}
        self.expertises = {}
        self.publications = []
        self.expertises = {}
    
    # Ajout d'un user, retour pour MonID
    def addProfil(self,user,interets,follow=False):
        myProfil = User(self,user[0],user[3],user[2],user[1],user[4],interets,user[5],follow)
        self.users[user[0]] = myProfil
                
        return myProfil
    
    def addPost(self,post,user,interets):
        poster = User(self,user[0],user[3],user[2],user[1],user[4],interets,user[5])
        publication = Publication(self,post[0],user[0],post[1],post[5],post[3],post[4])
        self.users[user[0]] = poster
        self.publications.append(publication)
        
        return publication

    def addInterests(self,sujetNom,sujetID):
        self.interets[sujetNom] = sujetID
        
    def updateFollow(self,user):
        if user in self.users:
            self.users[user].follow = True
            
    def addExpertise(self,sujetNom,sujetID):
        self.expertises[sujetNom] = sujetID

    def addExpertise(self,sujetNom,sujetID):
        self.expertises[sujetNom] = sujetID