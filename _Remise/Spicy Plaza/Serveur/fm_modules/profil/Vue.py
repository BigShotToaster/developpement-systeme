## -*- coding: UTF-8 -*- 

from tkinter import *
from SETTINGS import *
import re
from tkinter.ttk import Treeview
from datetime import date, time, datetime
from PIL import Image

from tkinter.filedialog import askopenfilename

import random
from optparse import Option

class Vue():
    def __init__(self,parent):
        self.parent=parent
        self.root=Tk()
        self.root.title('Spicy Plaza - Profil')
        self.root.configure(background=COLOR_WHITE)
        
        self.root.geometry(str(SCREEN_DIM_X)+"x"+str(SCREEN_DIM_Y))
        self.scrollY = 0
        self.printY = 0
        
        self.photosAvatar = {}
        
        # Déclaration de tous les Frames principaux
        self.topPanel = Frame()
        self.leftPanel = Canvas()
        self.rightPanel = Frame()
        self.centerPanel = Frame()
        
        
    def iniModule(self,publications,users,myProfil,interets, expertises):
        # Destruction de tous les frames principaux, aux cas ou un appel serait refait vers iniModule
        self.leftPanel.destroy()
        self.topPanel.destroy()
        self.rightPanel.destroy()
        self.centerPanel.destroy()
        
        self.topPanel=Frame(self.root,width=SCREEN_DIM_X,height=50, bg = COLOR_YELLOW)      
        self.topPanel.pack(fill = BOTH, side = TOP)
        
        self.img = PhotoImage(file="./modules/profil/img/SpicyPlaza_line.png")  
        label = Label(self.topPanel,image=self.img,bg=COLOR_YELLOW)
        label.pack(side=LEFT,padx=60)
        
        if self.parent.UserID == self.parent.MyID:
            self.img_refresh = PhotoImage(file="./modules/profil/img/refresh.png")
        else:
            self.img_refresh = PhotoImage(file="./modules/profil/img/back.png")
        
        self.refreshBtn = Button(self.topPanel,cursor="hand2",image=self.img_refresh, font = "GENERALFONT 12 bold underline",relief=FLAT,bd = 0, bg=COLOR_YELLOW,fg="#ffffff",activebackground=COLOR_YELLOW,activeforeground="#e8e8e8",command=self.parent.backToMyProfil)
        self.refreshBtn.pack(padx = 10,side=RIGHT,pady=10)
        
        self.messagerieBtn = Button(self.topPanel,cursor="hand2",text="Vitrine", font = "GENERALFONT 12 bold underline",relief=FLAT,bd = 0, bg=COLOR_YELLOW,fg="#ffffff",activebackground=COLOR_YELLOW,activeforeground="#e8e8e8", command=self.parent.ouvertureModuleVitrine)
        self.messagerieBtn.pack(padx = 10,side=RIGHT,pady=10)
        
        self.leftPanel=Frame(self.root,width=SCREEN_DIM_X/4,height=SCREEN_DIM_Y,  bg=COLOR_WHITE)      
        self.leftPanel.pack(side = LEFT, fill=BOTH)
        
        Label(self.leftPanel,text="Dernières publications",bg="#ffffff",font = "GENERALFONT 12 bold",fg=COLOR_TXT).pack(side=TOP,pady=5,padx=15,fill=BOTH)
        
        button1 = Button(self.leftPanel,cursor="hand2", text = "Revenir en haut", command = lambda:self.updateCanvas(publications))
        button1.configure(font = "GENERALFONT 12 bold",relief=FLAT,bd = 0, bg="#e05841",fg="#ffffff",activebackground="#e26a56",activeforeground="#e8e8e8")
        button1.pack(side = BOTTOM,padx=15,fill=X, pady=10)
        
        self.leftCanvevas=Canvas(self.leftPanel,width=SCREEN_DIM_X/4,height=SCREEN_DIM_Y,bg=COLOR_WHITE,bd=0,highlightthickness=0,scrollregion=(0, 0, 0, self.scrollY))       
        self.leftCanvevas.pack(side = BOTTOM,fill=BOTH)
        
        self.leftCanvevas.bind("<MouseWheel>", self.OnMouseWheel)
        
        
        # Il est possible que nous devions intégrer les Frames qui ont besoin d'être scrollable dans un canvas avec create-window
        # Puisque les Frames ne possèdent pas les attributs nécessaires pour scroller
        self.rightPanel=Frame(self.root,width=SCREEN_DIM_X/4,height=SCREEN_DIM_Y,  bg=COLOR_WHITE)      
        self.rightPanel.pack(side = RIGHT, fill=BOTH)
        
        
        self.followPanel = Frame(self.rightPanel,width=SCREEN_DIM_X/4,height=SCREEN_DIM_Y/2,bg=COLOR_WHITE)
        self.followPanel.pack(side = TOP, fill=BOTH,padx=15)
        Label(self.followPanel,text="Followed",bg="#ffffff",font = "GENERALFONT 12 bold",fg=COLOR_TXT).pack(side=TOP,pady=5,fill=BOTH)
        
        self.followCanvas = Canvas()
        
        
        self.usersPanel = Frame(self.rightPanel,width=SCREEN_DIM_X/4,height=SCREEN_DIM_Y/2,bg=COLOR_WHITE)
        self.usersPanel.pack(side = BOTTOM, fill=BOTH,padx=15)
        Label(self.usersPanel,text="Connaissez-vous?",bg="#ffffff",font = "GENERALFONT 12 bold",fg=COLOR_TXT).pack(side=TOP,pady=5,fill=BOTH)
        
        self.usersCanvas = Canvas()
        
        self.printCenter(users, interets, expertises)
        
        self.updateCanvas(publications)
        self.printFollowed(users, interets, expertises)
        Label(self.rightPanel,height=0,bg=COLOR_WHITE).pack(pady=0,fill=X)
        self.printUsers(users, interets, expertises)
        
        # Recentrage de la fenêtre, à appeler après l'avoir rempli
        self.center(self.root)
        
    def printCenter(self, users, interets, expertises):
        self.centerPanel.destroy()
        
        self.centerPanel=Frame(self.root,bg=COLOR_WHITE)
        self.centerPanel.pack(side = TOP,expand=TRUE, fill = BOTH)
        
        user = users[self.parent.UserID]
        
        self.boutonFrame = Frame(self.centerPanel,bg=COLOR_WHITE,height=SCREEN_DIM_X*.2)
        self.boutonFrame.pack(side = TOP, fill = BOTH)
        
        
        if self.parent.UserID == self.parent.MyID:
            buttonContacter = Button(self.boutonFrame, text = "Contacter")
            buttonContacter.configure(font = "GENERALFONT 12 bold",cursor="hand2",relief=FLAT,bd = 0,padx=15, bg=COLOR_TXT,fg="#ffffff",activebackground=COLOR_TXT,activeforeground="#e8e8e8")
            buttonContacter.pack(side=LEFT,pady=5)    
           
            buttonFollow = Button(self.boutonFrame, text = "Follow")
            buttonFollow.configure(font = "GENERALFONT 12 bold",cursor="hand2",relief=FLAT,bd = 0,padx=15, bg=COLOR_TXT,fg="#ffffff",activebackground=COLOR_TXT,activeforeground="#e8e8e8")
            buttonFollow.pack(side=RIGHT,pady=5)
        else:
            if user.follow:
                txtBtn = "UnFollow"
                bg = COLOR_YELLOW
                aBg = COLOR_LIGHYELLOW
            else:
                txtBtn = "Follow"
                bg = COLOR_RED
                aBg = COLOR_LIGHRED
            
        
            buttonContacter = Button(self.boutonFrame, text = "Contacter",command=lambda:self.parent.ouvertureModuleMessagerie("messagerie",user.id))
            buttonContacter.configure(font = "GENERALFONT 12 bold",cursor="hand2",relief=FLAT,bd = 0,padx=15, bg=COLOR_GREEN,fg="#ffffff",activebackground="#e26a56",activeforeground="#e8e8e8")
            buttonContacter.pack(side=LEFT,pady=5)    
           
            buttonFollow = Button(self.boutonFrame, text = txtBtn,command=lambda:self.parent.followByID(user.id))
            buttonFollow.configure(font = "GENERALFONT 12 bold",cursor="hand2",relief=FLAT,bd = 0,padx=15, bg=bg,fg="#ffffff",activebackground=aBg,activeforeground="#e8e8e8")
            buttonFollow.pack(side=RIGHT,pady=5)

        self.profilImageFrame = Frame(self.centerPanel,bg=COLOR_WHITE)         
        
        if users[self.parent.UserID].localPath != None:
            path = users[self.parent.UserID].localPath
        elif self.parent.UserID == self.parent.MyID:
            path = "./modules/profil/img/profil_small.png"
        else:
            path = "./modules/profil/img/profil_blue_small.png"
        
        im_name = user.prenom+user.nom+str(user.id)+".ppm"
        
        im_temp = Image.open(path)
        im_temp = im_temp.resize((150, 150), Image.ANTIALIAS)
        im_temp.save("./tmp/medium/"+im_name, "ppm")
        
        self.avatar = PhotoImage(file="./tmp/medium/"+im_name)  
        
        if self.parent.UserID == self.parent.MyID:
            self.avatarButton = Button(self.profilImageFrame,cursor="hand2",image=self.avatar,bd=4,relief=FLAT,bg=COLOR_WHITE,command=self.openImage)
            self.avatarButton.pack()
            
            self.avatarButton.bind("<Enter>", self.on_enter)
            self.avatarButton.bind("<Leave>", self.on_leave)
        else:
            avatarLabel = Label(self.profilImageFrame,image=self.avatar)
            avatarLabel.pack(padx=4,pady=4)
      
        self.profilImageFrame.pack(side = TOP, fill = BOTH)
        
        self.infosFrame = Frame(self.centerPanel,bg="white",height=SCREEN_DIM_X*.2)
        self.infosFrame.pack(side = TOP, fill = BOTH, pady = 10)

        label = Label(self.infosFrame,text = user.prenom+" "+user.nom,bg="white",font = "GENERALFONT 12 bold",width=25,anchor=E).grid(row=0, column=0)      
  
        label = Label(self.infosFrame,text = user.email,bg="white",fg=COLOR_TXT,font = "GENERALFONT 10 bold",width=30,anchor=W).grid(row=0, column=1)   
           
        self.infosFrame.columnconfigure(0, weight = 1)
        self.infosFrame.columnconfigure(1, weight = 1)
        
        self.boutonMenuFrame = Frame(self.centerPanel,bg=COLOR_WHITE,height=SCREEN_DIM_X*.2)
        self.boutonMenuFrame.pack(side = TOP, fill = BOTH, pady = (0, 10))
       
        bg = COLOR_RED
        aBg = COLOR_LIGHRED
        
        self.logo_info = PhotoImage(file="./modules/profil/img/menu_logo_info.png")
        
        buttonInfos = Button(self.boutonMenuFrame, image = self.logo_info)        
        buttonInfos.configure(relief=FLAT,bd = 0,cursor="hand2",padx=15, bg=COLOR_WHITE)
        buttonInfos.grid(row = 0, column = 1)  
        
        self.logo_cv = PhotoImage(file="./modules/profil/img/menu_logo_cv.png")

        buttonCV = Button(self.boutonMenuFrame, image = self.logo_cv)        
        buttonCV.configure(relief=FLAT,bd = 0,cursor="hand2",padx=15, bg=COLOR_WHITE)
        buttonCV.grid(row = 0, column = 2)  
        
        self.logo_portfolio = PhotoImage(file="./modules/profil/img/menu_logo_portfolio.png")
        
        buttonPortfolio = Button(self.boutonMenuFrame, image = self.logo_portfolio)        
        buttonPortfolio.configure(relief=FLAT,bd = 0,cursor="hand2",padx=15, bg=COLOR_WHITE)
        buttonPortfolio.grid(row = 0, column = 3)    
        
        self.boutonMenuFrame.columnconfigure(1, weight = 1)
        self.boutonMenuFrame.columnconfigure(2, weight = 1)
        self.boutonMenuFrame.columnconfigure(3, weight = 1)
        
        self.afficherDetails(users, interets, expertises)
    
    def afficherDetails(self, users, interets, expertises):
        #self.detailsFrame.destroy()
        
        user = users[self.parent.UserID]
        
        self.detailsFrame = Frame(self.centerPanel,bg="white",height=SCREEN_DIM_X*.8)
        self.detailsFrame.pack(side = TOP, fill = BOTH, expand = TRUE)
        self.detailsFrame.bind("<MouseWheel>", self.OnMouseWheel)
        
        
        self.boutonModifFrame = Frame(self.detailsFrame,bg="white",height=40)
        self.boutonModifFrame.pack(side = TOP, fill = BOTH)   
             
        if self.parent.UserID == self.parent.MyID:
            self.logo_modify = PhotoImage(file="./modules/profil/img/logo_modify.png")
            
            buttonModifier = Button(self.boutonModifFrame, image = self.logo_modify, command=lambda: self.modifierDetails(users, interets, expertises))        
            buttonModifier.configure(relief=FLAT,bd = 0,padx=15, cursor="hand2",bg="white",activebackground="white")
            
            buttonModifier.pack(side = RIGHT, pady = 5, padx = 5)
        
        titreMenu = Label(self.detailsFrame,text="Informations générales",bg="#ffffff",font = "GENERALFONT 17 italic bold",fg=COLOR_TXT)
        titreMenu.pack(side=TOP,padx=20)

        separation = Label(self.detailsFrame,text="________________________________________",bg="#ffffff",font = "GENERALFONT 14 bold",fg=COLOR_TXT)
        separation.pack(side=TOP,padx=20)
               
        nomprenom = Label(self.detailsFrame,text=user.prenom+" "+user.nom,bg="#ffffff",font = "GENERALFONT 15 bold")
        nomprenom.pack(side=TOP,padx=20, pady = (20, 0))
        
        email = Label(self.detailsFrame,text= user.email,bg="#ffffff",font = "GENERALFONT 13 bold",fg=COLOR_TXT)
        email.pack(side=TOP,padx=20)
        
        self.detailsContenu = Frame(self.detailsFrame,bg="white",height=SCREEN_DIM_X*.1,width=SCREEN_DIM_X)
        self.detailsContenu.pack(side = TOP, fill = BOTH) 

        
        label = Label(self.detailsContenu,text = "Status :",bg="white",font = "GENERALFONT 13 bold",width=25,anchor=E).grid(row=1, column=0, pady = (20, 5))      
        label = Label(self.detailsContenu,text = user.status,bg="white",fg=COLOR_TXT,font = "GENERALFONT 11 bold",width=30,anchor=W).grid(row=1, column=1, pady = (20, 5))
        
        
        
        label = Label(self.detailsContenu,text = "Domaine :",bg="white",font = "GENERALFONT 13 bold",width=25,anchor=E).grid(row=2, column=0, pady = 5)      
        label = Label(self.detailsContenu,text = user.expertise,bg="white",fg=COLOR_TXT,font = "GENERALFONT 11 bold",width=30,anchor=W).grid(row=2, column=1, pady = 5) 
        
        label = Label(self.detailsContenu,text = "Intérêts :",bg="white",font = "GENERALFONT 13 bold",width=25,anchor=E).grid(row=3, column=0, pady = (5, 0))
        
        #boucle qui récupère les intérêts
        for i,interet in enumerate(user.interets):
            label = Label(self.detailsContenu,text = str(interet[0]),bg="white",fg=COLOR_TXT,font = "GENERALFONT 11 bold",width=30,anchor=W)
            if i == 0 :
                label.grid(row = i + 3, column=1, pady = (5, 0))
            else :
                label.grid(row = i + 3, column=1)
        
        self.detailsContenu.rowconfigure(0, weight = 1)
        self.detailsContenu.rowconfigure(1, weight = 1)
    
    def modifierDetails(self, users, interets, expertises):
        self.detailsFrame.destroy()
        
        user = users[self.parent.UserID]        
        
        self.detailsFrame = Frame(self.centerPanel,bg=COLOR_YELLOW,height=SCREEN_DIM_X*.8)
        self.detailsFrame.pack(side = TOP, fill = BOTH, expand = TRUE)
        self.detailsFrame.bind("<MouseWheel>", self.OnMouseWheel)
        
        self.boutonModifFrame = Frame(self.detailsFrame,bg=COLOR_YELLOW,height=40)
        self.boutonModifFrame.pack(side = TOP, fill = BOTH)   
        
        self.logo_exit = PhotoImage(file="./modules/profil/img/logo_exit.png")

        buttonModifier = Button(self.boutonModifFrame, image = self.logo_exit, command=lambda: self.quitterModifier(users, interets, expertises))        
        buttonModifier.configure(relief=FLAT,bd = 0,padx=15, cursor="hand2",bg=COLOR_YELLOW,activebackground=COLOR_YELLOW)                
        buttonModifier.pack(side = RIGHT, pady = 5, padx = 5)        
       
        titreMenu = Label(self.detailsFrame,text="Informations générales",bg=COLOR_YELLOW,font = "GENERALFONT 17 italic bold",fg="white")
        titreMenu.pack(side=TOP,padx=20)

        separation = Label(self.detailsFrame,text="________________________________________",bg=COLOR_YELLOW,font = "GENERALFONT 14 bold",fg="white")
        separation.pack(side=TOP,padx=20)
        
        self.detailsContenu = Frame(self.detailsFrame,bg=COLOR_YELLOW,height=SCREEN_DIM_X*.1)
        self.detailsContenu.pack(side = TOP, fill = BOTH) 

        
        label = Label(self.detailsContenu,text = "Nom :",bg=COLOR_YELLOW,fg="white",font = "GENERALFONT 13 bold",width=25,anchor=E).grid(row=1, column=0, pady = (20, 5))      
        #label = Label(self.detailsContenu,text = user.nom,bg="white",fg=COLOR_TXT,font = "GENERALFONT 11 bold",width=30,anchor=W).grid(row=1, column=1, pady = (20, 5)) 

        varNom = StringVar(self.root)
        varNom.set(user.nom)
        self.entryNom = Entry(self.detailsContenu,textvariable=varNom)
        self.entryNom.grid(row=1, column=1, pady = (20, 5))

        label = Label(self.detailsContenu,text = "Prénom :",bg=COLOR_YELLOW,fg="white",font = "GENERALFONT 13 bold",width=25,anchor=E).grid(row=2, column=0, pady = 5)      
        #label = Label(self.detailsContenu,text = user.prenom,bg="white",fg=COLOR_TXT,font = "GENERALFONT 11 bold",width=30,anchor=W).grid(row=2, column=1, pady = 5) 
       
        varPrenom = StringVar(self.root)
        varPrenom.set(user.prenom)
        self.entryPrenom = Entry(self.detailsContenu,textvariable=varPrenom)
        self.entryPrenom.grid(row=2, column=1, pady = 5) 
       
        label = Label(self.detailsContenu,text = "Email :",bg=COLOR_YELLOW,fg="white",font = "GENERALFONT 13 bold",width=25,anchor=E).grid(row=3, column=0, pady = 5)      
        #label = Label(self.detailsContenu,text = user.email,bg="white",fg=COLOR_TXT,font = "GENERALFONT 11 bold",width=30,anchor=W).grid(row=3, column=1, pady = 5) 
        
        varEmail = StringVar(self.root)
        varEmail.set(user.email)
        self.entryEmail = Entry(self.detailsContenu,textvariable=varEmail)
        self.entryEmail.grid(row=3, column=1, pady = 5) 
        
        label = Label(self.detailsContenu,text = "Status :",bg=COLOR_YELLOW,fg="white",font = "GENERALFONT 13 bold",width=25,anchor=E).grid(row=4, column=0, pady = 5)      
        #label = Label(self.detailsContenu,text = user.status,bg="white",fg=COLOR_TXT,font = "GENERALFONT 11 bold",width=30,anchor=W).grid(row=1, column=1, pady = (20, 5))
        
        varStatus = StringVar(self.root)
        varStatus.set(user.status)
        self.entryStatus = Entry(self.detailsContenu,textvariable=varStatus)
        self.entryStatus.grid(row=4, column=1, pady = 5) 
        
        label = Label(self.detailsContenu,text = "Domaine :",bg=COLOR_YELLOW,fg="white",font = "GENERALFONT 13 bold",width=25,anchor=E).grid(row=5, column=0, pady = 5)      
        #label = Label(self.detailsContenu,text = user.expertise,bg="white",fg=COLOR_TXT,font = "GENERALFONT 11 bold",width=30,anchor=W).grid(row=2, column=1, pady = 5) 

        self.expertiseNom = StringVar(self.root)
        self.expertiseNom.set(user.expertise)  
        self.expertise = OptionMenu(self.detailsContenu, self.expertiseNom, *expertises)
        self.expertise.configure(font = "GENERALFONT 12 bold",relief=FLAT,bd = 0, fg=COLOR_TXT,activeforeground=COLOR_TXTACTIVE,bg="#ffffff",activebackground="#ffffff")
        self.expertise.grid(row = 5, column=1, pady = (5, 0))
                        
        
        self.dictOptionMenu = {} 
        self.dictOptionValue = {}               
        
        listKeys = ["None","None","None"]
        
        for i,interet in enumerate(user.interets):
            listKeys[i]=interet
            
                #boucle qui récupère les intérêts
        for i in range(3):
            
            label = Label(self.detailsContenu,text = "Intérêt " + str(i + 1) + ": ",bg=COLOR_YELLOW,fg="white",font = "GENERALFONT 13 bold",width=25,anchor=E) 
                
            text = listKeys[i]

            interetdefault = StringVar()
            interetdefault.set(text)  
            self.dictOptionValue[i] = interetdefault
            
            interet = OptionMenu(self.detailsContenu, interetdefault, *interets)
            interet.configure(font = "GENERALFONT 12 bold",relief=FLAT,bd = 0, fg=COLOR_TXT,activeforeground=COLOR_TXTACTIVE,bg="#ffffff",activebackground="#ffffff")
            
            self.dictOptionMenu[i] = interet
 
            if i == 0 :
                label.grid(row = i + 6, column=0, pady = (15, 0))
                interet.grid(row = i + 6, column=1, pady = (15, 0))
            else :
                label.grid(row = i + 6, column=0)
                interet.grid(row = i + 6, column=1)

        save = Button(self.detailsFrame, text= "Save", font = "GENERALFONT 13 bold italic", relief=RIDGE,bd=0, bg="#e05841",fg="#ffffff",activebackground="#e26a56",activeforeground="#e8e8e8",command = self.updateProfil)

        save.configure(width = 10, height = 1)
        save.pack(side = TOP, pady = 25, padx = 5)

        self.detailsContenu.rowconfigure(0, weight = 1)
        self.detailsContenu.rowconfigure(1, weight = 1)
        
    
    
    def updateProfil(self):
        prenom = self.entryPrenom.get()
        nom = self.entryNom.get()
        email = self.entryEmail.get()
        status = self.entryStatus.get()
        expertise = self.expertiseNom.get()
        
        interest = []
        for i, stringVar in enumerate(self.dictOptionValue.values()):
            text = stringVar.get().strip("()\",'")
            
            if text != "None":
                id = self.parent.modele.interets[text]
                if id not in interest:
                    interest.append(id)
        
        self.parent.updateProfil(prenom,nom,email,status,expertise,interest)
        

    
    def quitterModifier(self, users, interets, expertises):
        self.detailsFrame.destroy()
        self.afficherDetails(users, interets, expertises)
       
    def on_enter(self,event):
        event.widget.config(bg=COLOR_YELLOW)
    
    def on_leave(self,event):
        event.widget.config(bg=COLOR_WHITE)
                      
    def printFollowed(self,users, interets, expertises):
        self.followCanvas.destroy()
        
        followY = 0
        actualY = 425
        
        self.followCanvas=Canvas(self.followPanel,width=SCREEN_DIM_X/4,height=actualY,bd=0,highlightthickness=0,scrollregion=(0, 0, 0, followY))       
        self.followCanvas.pack(side = TOP,fill=BOTH)
        
        self.followedFrame = Frame(self.followCanvas,width=SCREEN_DIM_X/4,bg=COLOR_WHITE)
        self.followedFrame.pack(fill=BOTH)
        
        window = self.followCanvas.create_window(0,0,window=self.followedFrame,anchor=NW,width=SCREEN_DIM_X/4,tags=("followed",))
        
        self.followCanvas.bind("<MouseWheel>", self.OnMouseWheel)
        self.followedFrame.bind("<MouseWheel>", self.OnMouseWheel)
        
        if users != None:
            followed = []
            for user in users.values():
                if user.follow == True:
                    followed.append(user)
            
            for user in followed:
                if user.id == self.parent.UserID:
                    bg = COLOR_YELLOW
                    color = 'white'
                else:
                    bg = 'white'
                    color = COLOR_TXTACTIVE
                
                # Image Avatar
                if user.localPath != None:
                    path = user.localPath
                else:
                    path = "./modules/profil/img/profil_blue_small.png"    
                
                im_name = user.prenom+user.nom+str(user.id)+".ppm"
                
                im_temp = Image.open(path)
                
                im_temp = im_temp.resize((30, 30), Image.ANTIALIAS)
                im_temp.save("./tmp/small/"+im_name, "ppm")
                self.photosAvatar[user.id] = PhotoImage(file="./tmp/small/"+im_name) 
                
                username = Button(self.followedFrame,image=self.photosAvatar[user.id],compound='left',padx=10,command=lambda:self.changeConvo(user.id),text=user.prenom+" "+user.nom,bg=bg,font="GENERALFONT 10 bold",fg=color,anchor=W,height=40,relief=FLAT,bd=0,activeforeground=COLOR_BLACK,activebackground=COLOR_LIGHRED,cursor="hand2")
                username.config(command=lambda j=user.id:self.changeUser(j))
                username.pack(fill=X,pady=5)
                username.bind("<MouseWheel>", self.OnMouseWheel)
                followY += 54
            
            self.followCanvas.config(scrollregion=(0, 0, 0, actualY))

            diff = followY - actualY
            if diff > 0:
                self.followCanvas.config(scrollregion=(0, 0, 0, actualY+diff))
                
    def printUsers(self,users, interets, expertises):
        self.usersCanvas.destroy()
        
        followY = 0
        actualY = 370
        
        self.usersCanvas=Canvas(self.usersPanel,width=SCREEN_DIM_X/4,height=actualY,bd=0,highlightthickness=0,scrollregion=(0, 0, 0, followY))       
        self.usersCanvas.pack(side = TOP,fill=BOTH)
        
        self.usersFrame = Frame(self.usersCanvas,width=SCREEN_DIM_X/4,height=actualY,bg=COLOR_WHITE)
        self.usersFrame.pack(fill=BOTH,expand=TRUE)
        
        window = self.usersCanvas.create_window(0,0,window=self.usersFrame,anchor=NW,width=SCREEN_DIM_X/4,height=actualY,tags=("followed",))
        
        self.usersCanvas.bind("<MouseWheel>", self.OnMouseWheel)
        self.usersFrame.bind("<MouseWheel>", self.OnMouseWheel)
        
        if users != None:
            others = []
            for user in users.values():
                if user.follow == False and user.id != self.parent.MyID:
                    others.append(user)
            
            for user in others:
                if user.id == self.parent.UserID:
                    bg = COLOR_YELLOW
                    color = 'white'
                else:
                    bg = '#e0e0e0'
                    color = COLOR_TXTACTIVE
                    
                # Image Avatar
                if user.localPath != None:
                    path = user.localPath
                else:
                    path = "./modules/profil/img/profil_blue_small.png"  
                
                im_name = user.prenom+user.nom+str(user.id)+".ppm"
                
                im_temp = Image.open(path)
                
                im_temp = im_temp.resize((30, 30), Image.ANTIALIAS)
                im_temp.save("./tmp/small/"+im_name, "ppm")
                self.photosAvatar[user.id] = PhotoImage(file="./tmp/small/"+im_name) 
                
                username = Button(self.usersFrame,image=self.photosAvatar[user.id],compound='left',padx=10,command=lambda:self.changeConvo(user.id),text=user.prenom+" "+user.nom,bg=bg,font="GENERALFONT 10 bold",fg=color,anchor=W,height=40,relief=FLAT,bd=0,activeforeground=COLOR_BLACK,activebackground=COLOR_LIGHRED,cursor="hand2")
                username.config(command=lambda j=user.id:self.changeUser(j))
                username.pack(fill=X,pady=5)
                username.bind("<MouseWheel>", self.OnMouseWheel)
                
                followY += 44
            
            self.usersCanvas.config(scrollregion=(0, 0, 0, actualY))

            diff = followY - actualY
            if diff > 0:
                self.usersCanvas.config(scrollregion=(0, 0, 0, actualY+diff))
                
    def changeUser(self,userID):
        self.parent.changeUser(userID)
        
        
    def refreshView(self,publications,users, interets, expertises):
        if self.parent.UserID == self.parent.MyID:
            self.img_refresh = PhotoImage(file="./modules/profil/img/refresh.png")
        else:
            self.img_refresh = PhotoImage(file="./modules/profil/img/back.png")
            
        self.refreshBtn.config(image=self.img_refresh)
        
        
        self.printCenter(users, interets, expertises)
        self.printFollowed(users, interets, expertises)
        self.printUsers(users, interets, expertises)
        self.updateCanvas(publications)
        
        
    def updateCanvas(self,publications,resetPosition=1):
        canvas = self.leftCanvevas
        self.printY = 0
        self.scrollY = 0
        canvas.delete("post")
        
        # Afficher toutes les publications
        for post in publications:
            self.printPost(post)
            
            # Si resetPosition est activé, ajouter 10 à la scrollRegion pour chaque publication
            if(resetPosition):
                canvas.config(scrollregion=(0, 0, 0, self.scrollY+10))
        canvas.config(scrollregion=(0, 0, 0, self.scrollY+10))
        
    def printPost(self,post,border="#e0e0e0"):
        # Déclaration des valeurs de la publication et du User
        canvas = self.leftCanvevas
        
        message = post.message
        id = post.id
        date = post.date
        
        # Setting de la date
        strDate=""
        maintenant = datetime.now()
        
        # Calcul pour soit afficher le nombre de minute si cela fait moins d'une heure, soit afficher la date complète
        if date.date() == maintenant.date() and maintenant.hour - date.hour < 1 :
            if maintenant.minute - date.minute > 0:
                strDate += str(maintenant.minute-date.minute)+"min"
            else:
                strDate += "<1min"
        else:
            if date.date() != maintenant.date():
                strDate += str(date.year)+"-"+str(date.month)+"-"+str(date.day)+" | "
            
            strDate += str(date.hour)+"h "
                
            if date.minute > 9:
                strDate += str(date.minute)
            else:
                strDate += "0"+str(date.minute)
        
        # Setting de la page
        canvas.update()
        canvasWidth = canvas.winfo_width()
        canvasHeight = canvas.winfo_height()


        color="#ffffff"
        left = 30
        top = self.printY+30    # Calculé selon la position Y d'impression
        right = int(canvasWidth)-30 
        padding = 10
        
        width = right-padding*3-left
        font="GENERALFONT 8"
        
        # texte sur canevas
        # Création d'un objet text sur le canevas afin de récupérer la hauteur nécessaire pour le texte
        publication = canvas.create_text(left+padding*2,top+padding*3,text=message,anchor=NW,width=width,tags=("post","text",))
        
        # Récupération des bounds de la fenêtre et calcul de la hauteur de la publication
        bounds = canvas.bbox(publication)
        height = (bounds[3] - bounds[1])
        if height < 60:
            height = 60
        # Calcul du bottom de la publication selon le top et la hauteur
        bott = top+height
        
        # texte dans window sur canevas
        # Création d'un widget Text avec les même paramètre que l'objet Text du Canevas plus haut
        # Ceci permettra d'avoir un text sélectionnable
        text = Text(canvas,fg="grey25",bd=0,font=font,selectbackground="#e57360")
        text.pack()
        text.insert(END,message)
        text.config(state=DISABLED)
        
        # Création d'un objet Window sur le Canevas permettant d'afficher des Widget Tkinter dans un espace géré par un canevas
        # Le scroll fonctionne pour ces objets aussi
        window = canvas.create_window(left+padding*1.5,top+padding*3,window=text,anchor=NW,width=width,height=height-30,tags=("post","window",))
        
        text.bind("<MouseWheel>", self.OnMouseWheel)
        
        # Impression du cadre visuel de la publication
        # Rectangle de publication
        canvas.create_rectangle(left,top,right,bott,fill=color,width=0,tags=("post","bg",))
        
        # Coins arrondis
        canvas.create_oval(right-padding,top-padding,right+padding,top+padding,fill=color,width=0,tags=("post","coin",))
        canvas.create_oval(left-padding,top-padding,left+padding,top+padding,fill=color,width=0,tags=("post","coin",))
        canvas.create_oval(right-padding,bott-padding,right+padding,bott+padding,fill=color,width=0,tags=("post","coin",))
        canvas.create_oval(left-padding,bott-padding,left+padding,bott+padding,fill=color,width=0,tags=("post","coin",))
        
        # Bordure 
        canvas.create_rectangle(left,top-padding,right,top,fill=color,width=0,tags=("post","border",))
        canvas.create_rectangle(left,bott,right,bott+padding+1,fill=color,width=0,tags=("post","border",))
        canvas.create_rectangle(left-padding,top,left,bott,fill=color,width=0,tags=("post","border",))
        canvas.create_rectangle(right,top,right+padding+1,bott,fill=color,width=0,tags=("post","border",))
        
        canvas.create_line(left,top+padding*1.5,right,top+padding*1.5,width=2,fill=border,tags=("post","element",))
        
        # Sujet 
        font="GENERALFONT 10 bold"
        
        mydict = self.parent.modele.interets
        sujet = list(mydict.keys())[list(mydict.values()).index(post.sujet)]
        canvas.create_text((left)+15,top+padding/3,text=sujet,font = font,anchor=W,fill=COLOR_TXT,tags=("post",))
        
        # Date
        font="GENERALFONT 8"
        canvas.create_text(right-padding,top+padding/3,text=strDate,anchor=E, font = font, fill="#999999",tags=("post","date",))
        
        # Supression de l'objet Text sur Canevas; Inutile maintenant que nous avons les hauteurs nécessaires
        canvas.delete("text")
        
        # Incrémentation du scroll et de la position actuelle d'impression
        self.scrollY = self.scrollY+height+50
        self.printY = self.printY+height+50
        
    
    def OnMouseWheel(self,event):
        _left_list = self.leftCanvevas.winfo_children()
        _followCanvas_list = self.followCanvas.winfo_children()
        _followed_list = self.followedFrame.winfo_children()
        _usersCanvas_list = self.usersCanvas.winfo_children()
        _users_list = self.usersFrame.winfo_children()
        
        count = 0
        if event.num == 5 or event.delta == -120:
            count =1
        if event.num == 4 or event.delta == 120:
            count =-1
            
        if event.widget == self.leftCanvevas or event.widget in _left_list:
            self.leftCanvevas.yview('scroll', count, 'units')
            self.leftCanvevas.update()  
        elif event.widget == self.followCanvas or event.widget in _followCanvas_list or event.widget in _followed_list:
            self.followCanvas.yview('scroll', count, 'units')
            self.followCanvas.update()  
        elif event.widget == self.usersCanvas or event.widget in _usersCanvas_list or event.widget in _users_list: 
            self.usersCanvas.yview('scroll', count, 'units')
            self.usersCanvas.update()  
        elif event.widget == self.centerPanel: 
            pass
        
    def openImage(self):
        Tk().withdraw() # we don't want a full GUI, so keep the root window from appearing
        filename = askopenfilename(filetypes=[("Images", "*.png *.jpg")])
        print(filename)
        if filename:
            self.parent.uploadImage(filename)  
    
    # Fonction pour centrer une fenêtre, à utiliser après qu'elle ait déjà été remplie
    def center(self,win):
        """
        centers a tkinter window
        :param win: the root or Toplevel window to center
        """
        win.update_idletasks()
        width = win.winfo_width()
        frm_width = win.winfo_rootx() - win.winfo_x()
        win_width = width + 2 * frm_width
        height = win.winfo_height()
        titlebar_height = win.winfo_rooty() - win.winfo_y()
        win_height = height + titlebar_height + frm_width
        x = win.winfo_screenwidth() // 2 - win_width // 2
        y = win.winfo_screenheight() // 2 - win_height // 2
        win.geometry('{}x{}+{}+{}'.format(width, height, x, y))
        win.deiconify()
