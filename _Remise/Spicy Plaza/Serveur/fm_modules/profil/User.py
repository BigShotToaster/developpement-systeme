from SETTINGS import *
    
class User():    
    def __init__(self,parent,id,email="test@email.com",nom="Smith",prenom="John",expertise="None",sujets=[""], pathImage = None,follow=False):
        self.parent = parent
        self.id = id
        self.nom = nom
        self.prenom = prenom
        self.email = email
        self.interets = sujets
        self.status = "Travailleur"
        self.expertise = expertise
        
        self.follow = follow
        self.pathImage = pathImage
        self.localPath = None
        
    def setLocalPath(self, pathImage):
        self.localPath = pathImage