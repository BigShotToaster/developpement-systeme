## -*- coding: UTF-8 -*- 

from tkinter import *
from SETTINGS import *
import re
from tkinter.ttk import Treeview
from datetime import date, time, datetime
from PIL import Image

import random

class Vue():
    def __init__(self,parent):
        self.parent=parent
        self.root=Tk()
        self.root.title('Spicy Plaza - La Vitrine')
        self.root.configure(background=COLOR_WHITE)
        
        self.root.geometry(str(SCREEN_DIM_X)+"x"+str(SCREEN_DIM_Y))
        self.scrollY = 0
        self.printY = 0
        
        # Déclaration de tous les Frames principaux
        self.topPanel = Frame()
        self.centerCanvevas = Canvas()
        self.profilPanel = Frame()
        self.yscrollbar = Scrollbar()
        self.userPanel = Frame()
        self.centerFrame = Frame()
        self.searchPanel = Frame()
        
        # Déclaration des variables String utilisés dans les Dropdown Menu
        self.tkvar = StringVar(self.root)
        self.tkpost = StringVar(self.root)

        # Dictionnaire utilisé pour la récupération d'un utilisateur lorsqu'on clique sur son nom
        self._userDic = {}
        self.smallPhotosAvatar = {}
        self.fullPhotosAvatar = {}
        
        self.root.bind("<MouseWheel>", self.OnMouseWheel)
        
        # NB : Lors de la création de Frame et de Widget avec pack, les paramètres side, expand et fill sont très importants
        # Side permet de choisir à quel côté le widget doit "coller". Le premier déclaré sur un side sera le plus proche. LEFT, RIGHT, TOP, BOTTOM
        # Expand permet de dire que le widget peut prendre plus d'espace, selon le contenu (WRAP_CONTENT). TRUE, FALSE
        # Fill permet d'étendre le widget à la dimension de son parent (MATCH_PARENT). X, Y, BOTH
        
    def iniModule(self,publications,users,myProfil,interets):
        # Destruction de tous les frames principaux, aux cas ou un appel serait refait vers iniModule
        self.centerCanvevas.destroy()
        self.topPanel.destroy()
        self.profilPanel.destroy()
        self.yscrollbar.destroy()
        self.userPanel.destroy()
        self.centerFrame.destroy()
        
        self.topPanel=Frame(self.root,width=SCREEN_DIM_X,height=50, bg = "#e05841")      
        self.topPanel.pack(fill = BOTH, side = TOP)
        
        self.img = PhotoImage(file="./modules/vitrine/img/SpicyPlaza_line.png")  
        label = Label(self.topPanel,image=self.img,bg="#e05841")
        label.pack(side=LEFT,padx=60)
        
        self.img_refresh = PhotoImage(file="./modules/vitrine/img/refresh.png")
        self.refreshBtn = Button(self.topPanel,cursor="hand2",image=self.img_refresh, font = "GENERALFONT 12 bold underline",relief=FLAT,bd = 0, bg="#e05841",fg="#ffffff",activebackground="#e05841",activeforeground="#e8e8e8", command=self.parent.refreshView)
        self.refreshBtn.pack(padx = 10,side=RIGHT,pady=10)
        
        self.messagerieBtn = Button(self.topPanel,cursor="hand2",text="Messagerie", font = "GENERALFONT 12 bold underline",relief=FLAT,bd = 0, bg="#e05841",fg="#ffffff",activebackground="#e05841",activeforeground="#e8e8e8", command=self.parent.ouvertureModule)
        self.messagerieBtn.pack(padx = 10,side=RIGHT,pady=10)
        
        
        self.profilPanel=Frame(self.root,width=SCREEN_DIM_X/4,height=SCREEN_DIM_Y,  bg=COLOR_WHITE)      
        self.profilPanel.pack(side = LEFT, pady=75,fill=BOTH)
        
        self.profilPanelContent = Frame(self.profilPanel,width=SCREEN_DIM_X/4+40,height=SCREEN_DIM_Y,bg=COLOR_WHITE)
        self.profilPanelContent.pack(fill=BOTH)
        
        # Création de la scrollbar à droite affectant le canevas
        self.yscrollbar = Scrollbar(self.root)
        self.yscrollbar.pack(side=RIGHT,fill=Y)
        
        self.userPanel=Frame(self.root,width=SCREEN_DIM_X/4,height=SCREEN_DIM_Y,  bg=COLOR_WHITE)      
        self.userPanel.pack(side = RIGHT, pady=75,fill=BOTH)
        
        self.userPanelContent = Frame(self.userPanel,width=SCREEN_DIM_X/4+4,height=SCREEN_DIM_Y,bg=COLOR_WHITE)
        self.userPanelContent.pack(expand=TRUE,fill=BOTH)
        
        self.centerFrame=Frame(self.root)
        self.centerFrame.pack(side = TOP, fill = BOTH)
        
        self.searchPanel=Frame(self.centerFrame,bg=COLOR_WHITE,height = 80,highlightcolor=COLOR_WHITE,highlightthickness=5)
        self.searchPanel.pack(side = TOP,fill=BOTH)
        self.printSearch(interets)
       
        # Création du canevas centrale
        # yscrollcommand sert à affecter la scrollbar créée plus haut
        # scrollregion permet de définir une région limitée (puisqu'un canevas est normalement illimité en dimension). Cela permet de setter la hauteur maximale du scroll. Pour l'instant, self.scrollY égal 0
        self.centerCanvevas=Canvas(self.centerFrame,height=SCREEN_DIM_Y,bg=COLOR_WHITE,bd=0,highlightthickness=0,yscrollcommand=self.yscrollbar.set,scrollregion=(0, 0, 0, self.scrollY))       
        self.centerCanvevas.pack(side = TOP, fill = BOTH)
        
        # Bind de la commande de la scrollbar à la vue Y du canevas
        self.yscrollbar.config(command=self.centerCanvevas.yview)
        
        self.centerCanvevas.bind("<Button-1>",self.mousePress)
        
        self.printProfil(myProfil,interets)
        
        self.updateView(publications,users)
        
        # Recentrage de la fenêtre, à appeler après l'avoir rempli
        self.center(self.root)
        
        self.root.bind("<Configure>", self.on_resize)
        self.height = self.root.winfo_height()
        self.width = self.root.winfo_width()
        
    def printSearch(self,interets):
        
        # Les dropdown menu prennent des listes en paramètres; interets est une liste, *interets veut dire "Intégrer tous les éléments"
        # Pour setter le texte par défaut, il faut créer un StringVar et le setter après
        self.tkvar.set(list(interets.keys())[0])
        sujetMenu = OptionMenu(self.searchPanel, self.tkvar, *interets)
        sujetMenu.configure(font = "GENERALFONT 12 bold",relief=FLAT,bd = 0, fg=COLOR_TXT,activeforeground=COLOR_TXTACTIVE,bg="#ffffff",activebackground="#ffffff")
        sujetMenu.pack(side=LEFT,padx=5,fill=BOTH)
        
   
        
        frameUser = Frame(self.searchPanel,bg="#ffffff")
        frameUser.pack(side=LEFT,fill=BOTH,expand=TRUE,padx=5)
        
        self.varName = StringVar(self.root)
        self.varName.set("Name")
        self.userSearch = Entry(frameUser,textvariable=self.varName)
        self.userSearch.configure(font = "GENERALFONT 12 bold",relief=FLAT,bd = 0, fg=COLOR_TXT,bg="#ffffff",width=40)
        self.userSearch.pack(side=LEFT,fill=X)
        
        self.img_search = PhotoImage(file="./modules/vitrine/img/loupe.png")
        
        goBtn = Button(self.searchPanel, image=self.img_search,command=self.searchOption)
        goBtn.configure(font = "GENERALFONT 12 bold",cursor="hand2",height=50,width=50,relief=FLAT,bd = 0, bg="#e05841",fg="#ffffff",activebackground="#e26a56",activeforeground="#e8e8e8")
        goBtn.pack(side=RIGHT,padx=5)
        
    def printProfil(self,myProfil,interets):
        self.profilPanelContent.destroy()
        self.profilPanelContent = Frame(self.profilPanel,width=SCREEN_DIM_X/4+40,height=SCREEN_DIM_Y,bg=COLOR_WHITE)
        self.profilPanelContent.pack(fill=BOTH)
        
        if myProfil.localPath != None:
            path = myProfil.localPath
        else:
            path = "./modules/vitrine/img/profil_small.png"
        
        im_name = myProfil.prenom+myProfil.nom+str(myProfil.id)+".ppm"
        
        im_temp = Image.open(path)
        im_temp = im_temp.resize((200, 200), Image.ANTIALIAS)
        im_temp.save("./tmp/full/"+im_name, "ppm")
        
        self.avatar = PhotoImage(file="./tmp/full/"+im_name)  
        
        
        label = Label(self.profilPanelContent,image=self.avatar,width=SCREEN_DIM_X/4,height=210)
        label.pack()
        
        self.profilInsideFrame=Frame(self.profilPanelContent,width=SCREEN_DIM_X/4,bg = '#ffffff')
        self.profilInsideFrame.pack(padx=50,fill=BOTH)
        
        username = Label(self.profilInsideFrame,font="GENERALFONT 12 bold",text=myProfil.prenom+" "+myProfil.nom,bg='#ffffff',fg="#6d6d6d",anchor=W,justify=LEFT,wraplength=176)
        username.pack(pady=10,padx=10,fill=X)
        
        Label(self.profilInsideFrame,height=0,bg=COLOR_WHITE).pack(pady=0,fill=X)
        
        Label(self.profilInsideFrame,font="GENERALFONT 10",text=myProfil.email,bg='#ffffff',fg="#6d6d6d",anchor=W,justify=LEFT,wraplength=176).pack(pady=5,padx=10,fill=X)
        
        Label(self.profilInsideFrame,font="GENERALFONT 10",text="Status: "+myProfil.status,bg='#ffffff',fg="#6d6d6d",anchor=W,justify=LEFT,wraplength=176).pack(pady=5,padx=10,fill=X)
        
        Label(self.profilInsideFrame,font="GENERALFONT 10",text="Domaine: "+myProfil.expertise,bg='#ffffff',fg="#6d6d6d",anchor=W,justify=LEFT,wraplength=176).pack(pady=5,padx=10,fill=X)
        
        Label(self.profilInsideFrame,font="GENERALFONT 10",text="Intérets: ",bg='#ffffff',fg="#6d6d6d",anchor=W).pack(pady=5,padx=10,fill=X)
        i = 0
        for interet in myProfil.interets:
            Label(self.profilInsideFrame,font="GENERALFONT 9",text=interet,bg='#ffffff',fg="#6d6d6d",anchor=W).pack(pady=1,padx=20,fill=X)
            i +=1
            if i == 3:
                break
            
        Label(self.profilInsideFrame,height=0,bg="#ffffff").pack(pady=0,fill=X)
        

        button1 = Button(self.profilPanelContent, text = "Publier", command = lambda:self.windowPublishPost(interets))
        button1.configure(font = "GENERALFONT 12 bold",cursor="hand2",relief=FLAT,bd = 0, bg="#e05841",fg="#ffffff",activebackground="#e26a56",activeforeground="#e8e8e8")
        button1.pack(padx=62,fill=X, pady=10)
        
        button2 = Button(self.profilPanelContent, text = "Modifier Profil", command=lambda:self.parent.ouvertureModule("profil",self.parent.MyID))
        button2.configure(font = "GENERALFONT 12 bold",cursor="hand2",relief=FLAT,bd = 0, bg=COLOR_YELLOW,fg="#ffffff",activebackground=COLOR_LIGHYELLOW,activeforeground="#e8e8e8")
        button2.pack(padx=62,fill=X, pady=10)
            
        
    def printUser(self,profil):
        self.deleteUser()
        self.userPanelContent = Frame(self.userPanel,width=SCREEN_DIM_X/4+40,height=SCREEN_DIM_Y,bg=COLOR_WHITE)
        self.userPanelContent.pack(fill=BOTH)
        
        if profil.localPath != None:
            path = profil.localPath
        else:
            path = "./modules/vitrine/img/profil_blue_small.png"
        
        im_name = profil.prenom+profil.nom+str(profil.id)+".ppm"
        
        im_temp = Image.open(path)
        im_temp = im_temp.resize((200, 200), Image.ANTIALIAS)
        im_temp.save("./tmp/full/"+im_name, "ppm")
        
        self.userAvatar = PhotoImage(file="./tmp/full/"+im_name)  
        
        label = Label(self.userPanelContent,image=self.userAvatar,width=SCREEN_DIM_X/4,height=210)
        label.pack(side=TOP)
        
        if profil.follow:
            txtBtn = "UnFollow"
            bg = COLOR_YELLOW
            aBg = COLOR_LIGHYELLOW
        else:
            txtBtn = "Follow"
            bg = COLOR_RED
            aBg = COLOR_LIGHRED
        
        self.btnFollow = Button(self.userPanelContent, text = txtBtn,command=lambda:self.parent.followByID(profil.id))
        self.btnFollow.configure(font = "GENERALFONT 12 bold",cursor="hand2",relief=FLAT,bd = 0, bg=bg,fg="#ffffff",activebackground=aBg,activeforeground="#e8e8e8")
        self.btnFollow.pack(side=TOP, padx=62,fill=BOTH, pady=10)
        
        self.btnContact = Button(self.userPanelContent, text = "Contacter",command=lambda:self.parent.ouvertureModule("messagerie",profil.id))
        self.btnContact.configure(font = "GENERALFONT 12 bold",cursor="hand2",relief=FLAT,bd = 0, bg="#97cb52",fg="#ffffff",activebackground="#e26a56",activeforeground="#e8e8e8")
        self.btnContact.pack(side=TOP, padx=62,fill=BOTH, pady=10)
        
        self.userInsideFrame=Frame(self.userPanelContent,width=SCREEN_DIM_X/4,bg = '#ffffff')
        self.userInsideFrame.pack(padx=50,expand=TRUE,fill=BOTH)
        
        
        Label(self.userInsideFrame,font="GENERALFONT 12 bold",text=profil.prenom+" "+profil.nom,bg='#ffffff',fg="#6d6d6d",anchor=W,justify=LEFT,wraplength=176).pack(pady=10,padx=10,fill=X)
        
        Label(self.userInsideFrame,height=0,bg=COLOR_WHITE).pack(pady=0,fill=X)
        
        Label(self.userInsideFrame,font="GENERALFONT 10",text=profil.email,bg='#ffffff',fg="#6d6d6d",anchor=W,justify=LEFT,wraplength=176).pack(pady=5,padx=10,fill=X)
        
        
        label = Label(self.userInsideFrame,font="GENERALFONT 10",text="Status: "+profil.status,bg='#ffffff',fg="#6d6d6d",anchor=W,justify=LEFT,wraplength=176).pack(pady=5,padx=10,fill=X)
     
        
        Label(self.userInsideFrame,font="GENERALFONT 10",text="Domaine: "+profil.expertise,bg='#ffffff',fg="#6d6d6d",anchor=W,justify=LEFT,wraplength=176).pack(pady=5,padx=10,fill=X)
        
        Label(self.userInsideFrame,font="GENERALFONT 10",text="Intérets: ",bg='#ffffff',fg="#6d6d6d",anchor=W).pack(pady=5,padx=10,fill=X)
        
        i = 0
        for interet in profil.interets:
            Label(self.userInsideFrame,font="GENERALFONT 9",text=interet,bg='#ffffff',fg="#6d6d6d",anchor=W).pack(pady=1,padx=20,fill=X)
            i +=1
            if i == 3:
                break
            
        Label(self.userInsideFrame,height=0,bg="#ffffff").pack(pady=0,fill=X)
        
        self.varName.set(profil.prenom +" " + profil.nom)
        
        button2 = Button(self.userPanelContent, text = "Voir le Profil", command=lambda:self.parent.ouvertureModule("profil",profil.id))
        button2.configure(font = "GENERALFONT 12 bold",cursor="hand2",relief=FLAT,bd = 0, bg=COLOR_YELLOW,fg="#ffffff",activebackground=COLOR_LIGHYELLOW,activeforeground="#e8e8e8")
        button2.pack(padx=62,fill=X, pady=10)
        
    def deleteUser(self):
        self.userPanelContent.destroy()
    
    def updateView(self,publications,users,resetPosition=1):
        self.printY = 0
        self.scrollY = 0
        self.centerCanvevas.delete("post")
        
        for user in users.values():
            im_name = user.prenom+user.nom+str(user.id)+".ppm"
                    
            if user.localPath != None:
                path = users[user.id].localPath
            elif user.id == self.parent.MyID:
                path = "./modules/vitrine/img/profil_small.png"
            else:
                path = "./modules/vitrine/img/profil_blue_small.png"        
            
            im_temp = Image.open(path)
            
            im_temp = im_temp.resize((30, 30), Image.ANTIALIAS)
            im_temp.save("./tmp/small/"+im_name, "ppm")
            self.smallPhotosAvatar[user.id] = PhotoImage(file="./tmp/small/"+im_name) 
        
        # Afficher toutes les publications
        for post in publications:
            self.printPost(post,users[post.posterID])
            
            # Si resetPosition est activé, ajouter 10 à la scrollRegion pour chaque publication
            if(resetPosition):
                self.centerCanvevas.config(scrollregion=(0, 0, 0, self.scrollY+10))
        self.centerCanvevas.config(scrollregion=(0, 0, 0, self.scrollY+10))
            
    
    def refreshView(self,publications,users,resetUser=1,resetPosition=1,myProfil=None,interets=None):
        self.centerCanvevas.delete("post")
        self.scrollY = 0
        self.printY = 0
        for post in publications:
            self.printPost(post,users[post.posterID])
            
            # Si resetPosition est activé, ajouter 10 à la scrollRegion pour chaque publication
            if(resetPosition):
                self.centerCanvevas.config(scrollregion=(0, 0, 0, self.scrollY+10))
        
        # Si ResetUser est à 1, supprimer l'espace droit de la fenêtre et vider la zone de recherche par email
        if resetUser == 1:
            self.deleteUser()
            self.userSearch.delete(0, END)
            self.varName.set("Name")
            
        self.printProfil(myProfil, interets)
        
        
    def printPost(self,post,poster,border="#e0e0e0"):
        # Déclaration des valeurs de la publication et du User
        name = poster.prenom+" "+poster.nom
        message = post.message
        profilId = poster.id
        id = post.id
        date = post.date
        follow = poster.follow
        
        # Setting de la date
        strDate=""
        maintenant = datetime.now()
        
        # Calcul pour soit afficher le nombre de minute si cela fait moins d'une heure, soit afficher la date complète
        if date.date() == maintenant.date() and maintenant.hour - date.hour < 1 :
            if maintenant.minute - date.minute > 0:
                strDate += str(maintenant.minute-date.minute)+"min"
            else:
                strDate += "<1min"
        else:
            if date.date() != maintenant.date():
                strDate += str(date.year)+"-"+str(date.month)+"-"+str(date.day)+" | "
            
            strDate += str(date.hour)+"h "
                
            if date.minute > 9:
                strDate += str(date.minute)
            else:
                strDate += "0"+str(date.minute)
        
        # Setting de la page
        self.centerCanvevas.update()
        canvasWidth = self.centerCanvevas.winfo_width()
        canvasHeight = self.centerCanvevas.winfo_height()


        color="#ffffff"
        left = 30
        top = self.printY+30    # Calculé selon la position Y d'impression
        right = int(canvasWidth)-30 
        padding = 10
        
        width = right-padding*3-left
        font="GENERALFONT 8"
        
        # texte sur canevas
        # Création d'un objet text sur le canevas afin de récupérer la hauteur nécessaire pour le texte
        publication = self.centerCanvevas.create_text(left+padding*2,top+padding*3,text=message,anchor=NW,width=width,tags=("post","text",))
        
        # Récupération des bounds de la fenêtre et calcul de la hauteur de la publication
        bounds = self.centerCanvevas.bbox(publication)
        height = (bounds[3] - bounds[1])+30

        # Calcul du bottom de la publication selon le top et la hauteur
        bott = top+height+20
        
        # texte dans window sur canevas
        # Création d'un widget Text avec les même paramètre que l'objet Text du Canevas plus haut
        # Ceci permettra d'avoir un text sélectionnable
        text = Text(self.centerCanvevas,fg="grey25",bd=0,font=font,selectbackground="#e57360")
        text.pack()
        text.insert(END,message)
        text.config(state=DISABLED)
        
        # Création d'un objet Window sur le Canevas permettant d'afficher des Widget Tkinter dans un espace géré par un canevas
        # Le scroll fonctionne pour ces objets aussi
        window = self.centerCanvevas.create_window(left+padding*2,top+padding*3,window=text,anchor=NW,width=width,height=height-30,tags=("post","window",))
        
        
        # Impression du cadre visuel de la publication
        # Rectangle de publication
        self.centerCanvevas.create_rectangle(left,top,right,bott,fill=color,width=0,tags=("post","bg",))
        
        # Coins arrondis
        self.centerCanvevas.create_oval(right-padding,top-padding,right+padding,top+padding,fill=color,width=0,tags=("post","coin",))
        self.centerCanvevas.create_oval(left-padding,top-padding,left+padding,top+padding,fill=color,width=0,tags=("post","coin",))
        self.centerCanvevas.create_oval(right-padding,bott-padding,right+padding,bott+padding,fill=color,width=0,tags=("post","coin",))
        self.centerCanvevas.create_oval(left-padding,bott-padding,left+padding,bott+padding,fill=color,width=0,tags=("post","coin",))
        
        # Bordure 
        self.centerCanvevas.create_rectangle(left,top-padding,right,top,fill=color,width=0,tags=("post","border",))
        self.centerCanvevas.create_rectangle(left,bott,right,bott+padding+1,fill=color,width=0,tags=("post","border",))
        self.centerCanvevas.create_rectangle(left-padding,top,left,bott,fill=color,width=0,tags=("post","border",))
        self.centerCanvevas.create_rectangle(right,top,right+padding+1,bott,fill=color,width=0,tags=("post","border",))
        
        self.centerCanvevas.create_line(left+padding*1,top+padding*2-1,right,top+padding*2-1,width=2,fill=border,tags=("post","element",))
        # self.centerCanvevas.create_oval(left-padding*2,top-padding*2,left+padding*2,top+padding*2,fill=color,outline=border,width=2,tags=("post","element",))
        
        # ID bubble
        if poster.id in self.smallPhotosAvatar:
            self.centerCanvevas.create_image(left+padding/2,top+padding/2,image=self.smallPhotosAvatar[poster.id],tags=("post","element",))
        
        #font="GENERALFONT 12 bold"
        #self.centerCanvevas.create_text(left,top,text=id, font = font, fill=COLOR_TXT,tags=("post","postID",))
        
        # Profil name
        font="GENERALFONT 10 bold"
        
        if(poster.id == self.parent.MyID):
            self.centerCanvevas.create_text(left+padding*3,top+padding/3,text=name,anchor=W,font = font, fill=COLOR_RED,tags=("post",))
        else:
            if follow:
                color = "#f9b450"
            else:
                color = COLOR_TXT   # Modification de la couleur du nom si c'est un usager que l'on suit ou nous-même
            # Ajout de l'usager au dictionnaire afin que le nom soit cliquable
            self._userDic[self.centerCanvevas.create_text(left+padding*3,top+padding/3,text=name,anchor=W,font = font,activefill=COLOR_TXTACTIVE, fill=color,tags=("post","username",))] = profilId
        
        
        mydict = self.parent.modele.interets
        sujet = list(mydict.keys())[list(mydict.values()).index(post.sujet)]
        self.centerCanvevas.create_text((right-left)/2,top+padding/3,text=sujet,font = font, fill=COLOR_TXT,tags=("post",))
        
        # Date
        font="GENERALFONT 8"
        self.centerCanvevas.create_text(right-padding,top+padding/3,text=strDate,anchor=E, font = font, fill="#999999",tags=("post","date",))
        
        # Supression de l'objet Text sur Canevas; Inutile maintenant que nous avons les hauteurs nécessaires
        self.centerCanvevas.delete("text")
        
        # Incrémentation du scroll et de la position actuelle d'impression
        self.scrollY = self.scrollY+height+70
        self.printY = self.printY+height+70
        
        
    def OnMouseWheel(self,event):
        count = 0
        if event.num == 5 or event.delta == -120:
            count =1
        if event.num == 4 or event.delta == 120:
            count =-1
        self.centerCanvevas.yview('scroll', count, 'units')
        self.centerCanvevas.update()
        
    def mousePress(self,event):
        x, y = event.x, self.centerCanvevas.canvasy(event.y)
        item=self.centerCanvevas.find_closest(x, y, 2)[0]
        if "username" in self.centerCanvevas.gettags(item): # Détection du tag username et récupération dans le dictionnaire du user sélectionné
            profilID = self._userDic[item]
            self.userSearch.delete(0, END)
            
            self.parent.showUser(profilID)
        else:
            self.deleteUser()
            self.userSearch.delete(0, END)
            self.varName.set("Name")
            
    def searchOption(self):
        sujet = self.tkvar.get()
        email = self.userSearch.get()
        self.parent.passSearch(sujet,email)
            

    def windowPublishPost(self,interets):
        self.publishPause = Toplevel(height=500, width=700)
        self.publishPause.title("Publier")
        self.publishPause.configure(background = 'grey25')
        
        self.tkpost.set(list(interets.keys())[1])

        """
        w = self.publishPause.winfo_screenwidth()
        h = self.publishPause.winfo_screenheight()
        size = tuple(int(_) for _ in self.publishPause.geometry().split('+')[0].split('x'))
        x = w/2 - size[0]/2
        y = h/2 - size[1]/2"""
        
        self.publishpostFrame = Frame(self.publishPause)
        self.publishpostFrame.configure(pady = 15, padx = 30, background = "#e05841")
        self.publishpostFrame.pack()
        
        self.textePublication = Text(self.publishpostFrame, height = 5, width = 55)
        self.textePublication.pack()
        
        self.boutonPostFrame = Frame(self.publishpostFrame)
        self.boutonPostFrame.configure(pady = 20, background =  "#e05841")
        self.boutonPostFrame.pack()
        
        label = Label(self.boutonPostFrame, text="Type de publication : ", font = "GENERALFONT 12 bold", fg = 'white', bg="#e05841")
        label.grid(padx = 15, column = 0, row = 0)
        
        sujetMenu = OptionMenu(self.boutonPostFrame, self.tkpost, *interets)
        sujetMenu.configure(font = "GENERALFONT 12 bold",relief=FLAT,bd = 0, fg=COLOR_TXT,activeforeground=COLOR_TXTACTIVE,bg="#ffffff",activebackground="#ffffff")
        sujetMenu.grid(column = 1, row = 0)
        
        sujetMenu["menu"].delete("None")
        
        b = Button(self.boutonPostFrame, text="Publier", cursor="hand2",font = "GENERALFONT 14", anchor = E, command = self.creationPost)
        #b.config(width=1) 
        b.grid(padx = 25, column = 2, row = 0)
        
        #Appel pour centrer la fenÃªtre
        self.center(self.publishPause)
        
    def creationPost(self):
        subject = self.tkpost.get()
        txt = self.textePublication.get(1.0, 'end-1c')      
        self.parent.creationPost(subject,txt)
        self.updatePourPost()
            
    def updatePourPost(self):
        self.publishPause.destroy()
        self.parent.refreshView()
        
    # Fonction pour centrer une fenêtre, à utiliser après qu'elle ait déjà été remplie
    def center(self,win):
        """
        centers a tkinter window
        :param win: the root or Toplevel window to center
        """
        win.update_idletasks()
        width = win.winfo_width()
        frm_width = win.winfo_rootx() - win.winfo_x()
        win_width = width + 2 * frm_width
        height = win.winfo_height()
        titlebar_height = win.winfo_rooty() - win.winfo_y()
        win_height = height + titlebar_height + frm_width
        x = win.winfo_screenwidth() // 2 - win_width // 2
        y = win.winfo_screenheight() // 2 - win_height // 2
        win.geometry('{}x{}+{}+{}'.format(width, height, x, y))
        win.deiconify()
    
    def changeFollow(self,txt,color):
        self.btnFollow.config(text=txt, bg=color)
        
    def on_resize(self,event):
        pass