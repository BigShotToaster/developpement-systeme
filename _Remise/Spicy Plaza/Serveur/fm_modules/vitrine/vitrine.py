# -*- coding: utf-8 -*-
# Version 
# 1

import xmlrpc.client
import os,os.path
import sys
import socket
from subprocess import Popen 

from SETTINGS import *
from Vue import *
from Modele import * 


class Controleur():
    def __init__(self,id=1):
        self.modele= Modele(self) 
        self.vue= Vue(self)
        self.server=None
        self.MyID = int(sys.argv[1])
        self.ip = sys.argv[2]
        self.loginclient(self.ip)
        
        # Ne faire la suite que si un server est connecté
        if self.server:
            self.getInterests()
            self.iniModule()
            self.vue.root.mainloop()
        
    def loginclient(self,ip="127.0.0.1",port="50017"):
        ad="http://"+ip+":"+port
        self.server=xmlrpc.client.ServerProxy(ad)
        print(self.server)    
    
    # Récupération de mon profil et de mes intérêts
    def getMyInfo(self):
        request = self.server.getUser(self.MyID)
        if request != -1:
            interets = self.server.getInteretsByID(request[0])
            profil = self.modele.addProfil(request,interets)
            self.downloadImage(self.modele.users[profil.id].pathImage, request[0])
            return profil
        return -1
    
    # Récupération des personnes que je suis dans un tableau
    # Rénitialisation de la valeur Follow dans tous les users
    # Initialisation de la valeur Follow à tous ceux dans le tableau
    def getMyFollow(self):
        request = self.server.getMyFollow(self.MyID)
        
        for user in self.modele.users.values():
            self.modele.users[user.id].follow = False
        
        if request != -1:
            for follow in request:
                self.modele.updateFollow(follow[0])
                
    # Setter un Follow ou UnFollow dans le server
    def followByID(self,userID):
        if self.modele.users[userID].follow:
            req = self.server.unfollow(self.MyID,userID)
            self.vue.changeFollow("Follow", "#e05841")
        else:
            req = self.server.follow(self.MyID,userID)
            self.vue.changeFollow("UnFollow","#f9b450")
        
        self.refreshView(0,0)
    
    # Récupération de l'ensemble des intérêts et remplissage du dictionnaire dans le modèle
    # Le nom est la clé et le id est la valeur
    def getInterests(self):
        request = self.server.getInterests()
        if request != -1:
            for sujet in request:
                self.modele.addInterests(sujet[1],sujet[0])
                
    def creationPost(self,sujet,txt):
        sujetID = self.modele.interets[sujet]
        self.server.addPublication(txt, self.MyID, sujetID)
    
    # Récupération des publications, poster et intérêts
    # Division 1) get à l'ouverture du programme et refresh 2) Get lors du défilement
    def getPublications(self,qty=10):
        if len(self.modele.publications) == 0:
            request = self.server.getPublications(self.MyID,qty,self.modele.searchOption[0],self.modele.searchOption[1])
            
            if request != None and request != -1:
                for post in request:
                    rep = self.server.getUser(post[2])
                    if rep != -1:
                        interets = self.server.getInteretsByID(rep[0])
                        
                        self.modele.addPost(post,rep,interets)
                        self.downloadImage(self.modele.users[rep[0]].pathImage, rep[0])

                    if post[0] == 0:
                        break
        elif self.modele.publications[-1].id > 0:
            request = self.server.getPublications(self.MyID,qty,self.modele.searchOption[0],self.modele.searchOption[1],self.modele.publications[-1].id)
            if request != None and request != -1:
                for post in request:
                    rep = self.server.getUser(post[2])
                    if rep != -1:
                        interets = self.server.getInteretsByID(rep[0])
                        self.modele.addPost(post,rep,interets)
                        self.downloadImage(self.modele.users[rep[0]].pathImage, rep[0])
                    if post[0] == 0:
                        break
                self.getMyFollow()
                self.vue.updateView(self.modele.publications,self.modele.users,0)
        
    
    def refreshView(self,resetUser=1,resetPosition=1):
        self.modele.passSearch("None","None")
        self.vue.tkvar.set("None")
        nbPublications = 0
        if resetPosition:
            nbPublications = 10
        else:
            nbPublications = len(self.modele.publications)
            
        self.modele.publications.clear()

        myProfil = self.getMyInfo()
        self.getPublications(nbPublications)
        self.getMyFollow()
        self.vue.refreshView(self.modele.publications,self.modele.users,resetUser,resetPosition,myProfil,self.modele.interets)
        
    
    def checkNew(self):
        if self.vue.centerCanvevas.yview()[1] > 0.9:
            self.getPublications(10)
        #self.vue.updateView(self.modele.publications,0)
        self.vue.root.after(1000,self.checkNew)
        
    def iniModule(self):
        myProfil = self.getMyInfo()
        if myProfil != -1:
            self.getPublications()
            self.getMyFollow()
            
            self.vue.iniModule(self.modele.publications,self.modele.users,myProfil,self.modele.interets)
            self.checkNew()
            
        else:
            print("Error")
        
    def showUser(self,id):
        self.vue.printUser(self.modele.users[id])

    def passSearch(self,sujet,email):
        self.modele.passSearch(sujet,email)  
        self.modele.publications.clear()
        myProfil = self.getMyInfo()
        self.getPublications()
        self.getMyFollow()
        self.vue.refreshView(self.modele.publications,self.modele.users,0,1,myProfil,self.modele.interets)
        pass
    
    def getImage(self):
        self.downloadImage(self.modele.users[1].pathImage, 1)
    
    def downloadImage(self, path, id):
        if path !='0':
            rep=self.server.requetefichier(path)
            lieu = ".\\img"
            if not os.path.exists(lieu):
                    os.mkdir(lieu) 
            lieu = lieu + "\\" + str(id)
            if not os.path.exists(lieu):
                os.mkdir(lieu) 
            list = os.listdir(lieu) # dir is your directory path
            numberFiles = len(list)
            lieu = lieu+"\\"+ str(0)+ ".jpg"
            fiche=open(lieu,"wb")
            
            fiche.write(rep.data)
            fiche.close() 
            self.modele.users[id].setLocalPath(lieu)
            
    
    def ouvertureModule(self, module="messagerie", idContact = -1):
        repRequeteModule = self.server.requetemodule(module)
        #print(repRequeteModule)
        if repRequeteModule:
            #print(repRequeteModule[0])
            cwd=os.getcwd()
            lieuApp="\\"+repRequeteModule[0]
            lieu=cwd+"\\modules"
            if not os.path.exists(lieu):
                os.mkdir(lieu) #plante s'il exist deja
            lieu=lieu+lieuApp
            #print(lieu)
            if not os.path.exists(lieu):
                os.mkdir(lieu) #plante s'il exist deja
            bonPath=repRequeteModule[1]
            #print(repRequeteModule[1])
            #print("contenu dossier/fichier", repRequeteModule[2])
            for i in repRequeteModule[2]:
                #print(i)
                if i[0]=="fichier":
                    nom=bonPath+i[1]
                    #print("fichier", nom)
                    rep=self.server.requetefichier(nom)
                    fiche=open(lieu+"\\"+i[1],"wb")
                    fiche.write(rep.data)
                    fiche.close()
                elif i[0]=="dossier":
                    if not os.path.exists(lieu+"\\"+i[1]):
                        os.mkdir(lieu+"\\"+i[1])
                elif i[0]=="sous-fichier":
                    nom=bonPath+i[1]+'\\'+i[2]
                    #print("sous-fichier", nom)
                    rep=self.server.requetefichier(nom)
                    fiche=open(lieu+"\\"+i[1]+"\\"+i[2],"wb")
                    fiche.write(rep.data)
                    fiche.close()
            chaineappli=lieu+lieuApp+".py"

            self.pid = Popen([sys.executable, chaineappli,str(self.MyID), str(idContact), self.ip],shell=0)
        else:
            print("RIEN")

if __name__ == '__main__':
    c=Controleur()