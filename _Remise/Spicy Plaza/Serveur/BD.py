# -*- coding: utf-8 -*-

import sqlite3
from _datetime import datetime
from _overlapped import NULL
from telnetlib import EL

class BD():
    
    def __init__(self,GENERATECONTENT=False):
        self.conn = sqlite3.connect('example.db')
        self.GENERATECONTENT = GENERATECONTENT
        self.creation_bd()
        
        
        
    def creation_bd(self):
        c = self.conn.cursor()
        
        if self.GENERATECONTENT:
            self.dropTables()
        
        #Creation Table expertises
        c.execute('''CREATE TABLE if not exists expertises 
                     (id integer not null primary key,
                      nom text) ''')
        
        ##Creation table usagers
        c.execute('''CREATE TABLE if not exists users 
                     (id integer not null primary key AUTOINCREMENT,
                      nom text,
                      prenom text,
                      password text,
                      status text,
                      email text
                      cv integer,
                      pathImage text,
                      abonnement text,
                      nombre_de_credit integer,
                      connected integer,
                      moduleDispo integer,
                      id_expertise integer,
                      FOREIGN KEY(moduleDispo) REFERENCES user_module(id),
                      FOREIGN KEY(id_expertise) REFERENCES expertises(id)) ''')
        
        
        ##Creation table follow
        c.execute('''CREATE TABLE if not exists follow 
                     (id integer not null primary key AUTOINCREMENT,
                      id_user integer not null,
                      followed_id integer not null)''')
        
        ##Creation de la table publication
        c.execute('''CREATE TABLE if not exists publication 
                     (id integer not null primary key AUTOINCREMENT,
                      message text,
                      publishdate text,
                      rank integer,
                      userID integer,
                      interetID integer,
                      FOREIGN KEY (userID) REFERENCES users(id),
                      FOREIGN KEY (interetID) REFERENCES interet(id))''')
        
        ##Creation de la table interets
        c.execute('''CREATE TABLE if not exists interet 
                     (id integer not null primary key AUTOINCREMENT,
                      nom text)''')
        
        ##Creation de la table d'association user_interets
        c.execute('''CREATE TABLE if not exists user_interet 
                     (user_id integer not null,
                      interet_id integer not null,
                      PRIMARY KEY(user_id,interet_id),
                      FOREIGN KEY (user_id) REFERENCES users(id),
                      FOREIGN KEY(interet_id) REFERENCES interet(id)) ''')
        
        
        ##Creation de la table portfolio
        c.execute('''CREATE TABLE if not exists porfolio 
                     (id integer not null primary key AUTOINCREMENT,
                      titre text,
                      expertise_en_lien integer,
                      description text,
                      imagePath text,
                      user_id integer,
                      FOREIGN KEY(expertise_en_lien) REFERENCES expertises(id),
                      FOREIGN KEY(user_id) REFERENCES users(id)) ''')
        
        #Creation Table CV
        c.execute('''CREATE TABLE if not exists cv 
                     (id integer not null primary key,
                      nom text,
                      prenom text,
                      email text,
                      tel text,
                      imagePath text,¸
                      body text) ''')
        
        #Creation Table Message
        c.execute('''CREATE TABLE if not exists messages 
                    (id integer not null primary key,
                     message text,
                     date text) ''')
        
        #Creation Table Scolarite
        c.execute('''CREATE TABLE if not exists scolarite 
                    (id integer not null primary key,
                     nom_ecole text,
                     titre_etude text) ''')
        
        #Table d'association cv_scolarite
        c.execute('''CREATE TABLE if not exists cv_scolarite 
                    (cv_id integer not null,
                     scolarite_id integer not null,
                     PRIMARY KEY (cv_id, scolarite_id),
                     FOREIGN KEY(cv_id) REFERENCES cv(id),
                     FOREIGN KEY(scolarite_id) REFERENCES scolarite(id) )''')
        
        #Table d'association messages_users
        c.execute('''CREATE TABLE if not exists messages_users 
                    (message_id integer not null,
                     poster_id integer not null, 
                     receiver_id integer not null, 
                     PRIMARY KEY (message_id, poster_id, receiver_id),
                     FOREIGN KEY(message_id) REFERENCES messages(id),
                     FOREIGN KEY(poster_id) REFERENCES users(id), 
                     FOREIGN KEY(receiver_id) REFERENCES users(id) )''')
        
        #Table module
        c.execute('''CREATE TABLE if not exists module 
                    (id integer not null primary key,
                    nom text,
                    version integer
                 )''')

        
        ##Creation table module
        c.execute('''CREATE TABLE if not exists user_module 
                    (id integer not null primary key AUTOINCREMENT,
                     user_id integer not null, 
                     module_id integer not null,
                     FOREIGN KEY(user_id) REFERENCES users(id),
                     FOREIGN KEY(module_id) REFERENCES module(id))''')
        
        self.conn.commit()
        
    def dropTables(self):
        c = self.conn.cursor()
        c.execute('DROP TABLE IF EXISTS users')
        c.execute('DROP TABLE IF EXISTS interet')
        c.execute('DROP TABLE IF EXISTS user_interet')
        c.execute('DROP TABLE IF EXISTS publication')
        c.execute('DROP TABLE IF EXISTS follow')
        c.execute('DROP TABLE IF EXISTS expertises')
        c.execute('DROP TABLE IF EXISTS messages')
        c.execute('DROP TABLE IF EXISTS messages_users')
        
        self.conn.commit()
        
     
    def getUser(self, id):
        c = self.conn.cursor() 
        c.execute('SELECT id, prenom,nom, email,(SELECT nom FROM expertises WHERE id = id_expertise),pathImage  FROM users  WHERE id = ?',(id,))
        result = c.fetchone()
        return result
    
    def insertUser(self,prenom,nom,password,email,expertise):
        c = self.conn.cursor()
        id = c.execute('''INSERT INTO users(nom,prenom,password,status,email,pathImage,abonnement,nombre_de_credit,connected,id_expertise) 
                     VALUES(?,?,?,?,?,?,?,?,?,?)''',(nom,prenom,password,'Travailleur',email,NULL,'Non','0','0',expertise,))
        
        self.conn.commit()             
        return id       
    
    def connectUser(self,id):
        c = self.conn.cursor()
        c.execute('UPDATE users SET connected = 1 WHERE id = ',(id,))
        self.conn.commit() 
        
    def disconnectUser(self,id):
        c = self.conn.cursor()
        c.execute('UPDATE users SET connected = 0 WHERE id = ',(id,))
        self.conn.commit()  
    
    def newGetPublications(self,postID=-1,qty=10,sujet="None",email="None",userID=None):
        c = self.conn.cursor()
        query = '''SELECT id,message,userID,publishdate,rank,interetID 
        FROM publication
        WHERE id > 0'''
        
        if postID != -1:
            query += ' AND id < '+str(postID)
            
        if sujet == "None" and email == "None":
            if userID:
                interets = self.getInteretUser(userID)
                if len(interets) > 0:
                    query += ' AND (interetID IN (SELECT interet_id FROM user_interet WHERE user_id = '+str(userID)+')'
                    query += ' OR userID IN (SELECT followed_id FROM follow WHERE id_user = '+str(userID)+'))'
        else:
            if sujet != "None":
                query += ' AND interetID IN (SELECT id FROM interet WHERE nom = "'+str(sujet)+ '")'
            
            
            if email != "None":
                check = []
                names = email.split();
                
                for name in names:
                    userIDs = self.getUsersByName(name)
                    print("name",userIDs)
                    if userIDs:
                        for id in userIDs:
                            check.append(id[0])
                    
                print("check",check)
                #check = self.getUserByEmail(email)[0]
                
                if check:
                    print(check)
                    if len(check) > 1:
                        query += ' AND userID IN '+str(tuple(check))+ ' '
                    else:
                        query += ' AND userID = '+str(check[0])+ ' '
    
        print(query)
        
        query += ' ORDER BY id DESC LIMIT '+str(qty)
        
        c.execute(query)   
        result = []
        for element in c:
            #print(element[0])
            result.append(element)
        return result 
    
    def getPublicationsByID(self,postID=-1,qty=10,userID=None):
        c = self.conn.cursor()
        query = '''SELECT id,message,userID,publishdate,rank,interetID 
        FROM publication
        WHERE id > 0'''
        
        if postID != -1:
            query += ' AND id < '+str(postID)
            
        query += ' AND userID = '+str(userID)
        
        
        query += ' ORDER BY id DESC LIMIT '+str(qty)

        
        c.execute(query)   
        result = []
        for element in c:
            #print(element[0])
            result.append(element)
        return result 
    
    def getUserByEmail(self,email):
        c = self.conn.cursor()
        c.execute('''SELECT id FROM users WHERE email = ? ''', (email,))
        result = c.fetchone()
        
        return result
    
    def getUsersByName(self,nom):
        c = self.conn.cursor()
        c.execute('''SELECT id FROM users WHERE nom = ? OR prenom = ?''', (nom,nom,))
        result = []
        for element in c:
            result.append(element)
        return result
    
    def getUserFullName(self, UserId):
        c = self.conn.cursor()
        c.execute('''SELECT prenom,nom FROM users WHERE id = ?''', (UserId,))
        result = []
        for element in c:
            result.append(element)
        return result
    
    def insertPublication(self, message, userID, interetID):
        c = self.conn.cursor()
        c.execute('''INSERT INTO publication(message,userID,interetID,publishdate,rank)
                     VALUES(?,?,?,?,?)''',(message, int(userID),int(interetID),datetime.now(),1,))
        self.conn.commit()
        return 0    
    
    def getExpertiseName(self, idExpertise):
        c = self.conn.cursor()
        c.execute('''SELECT nom FROM expertises WHERE id = ? ''', (idExpertise,))
        result = c.fetchone()
        return result
    
    def getExpertises(self):
        c = self.conn.cursor()
        c.execute('SELECT id,nom from expertises')   
        result = []
        for element in c:
            result.append(element)
        return result 
     
    def getExpertiseID(self,nomExpertise):
        c = self.conn.cursor()
        c.execute('SELECT id FROM expertises WHERE nom = ?', (nomExpertise,))   
        result = c.fetchone()
        return result   
        
    def insertExpertise(self, nom):
        c = self.conn.cursor()
        c.execute('INSERT INTO expertises(nom) VALUES(?)',(nom,))
        self.conn.commit()
        
    #Mettre le user online car il ne l'est pas lors de son insertion
    def putUserOnline(self, id):
        c = self.conn.cursor()
        c.execute('''UPDATE users SET connected = 1 WHERE id = ? ''', (id,))
        
    
    
    def getIdentifiers(self, email, password):
        c = self.conn.cursor()
        c.execute('SELECT * FROM users WHERE email = "'+ email +'" AND password = "'+ password +'" ')    
        check = c.fetchone()
        return check       
         
    
    def getUserTest(self):
        c = self.conn.cursor()
        c.execute('SELECT * FROM users')    
        check = c.fetchone()
        return check       
    
    def insertModule(self, moduleName, version):
        c = self.conn.cursor()
        c.execute('''INSERT INTO module(nom,version) 
                     VALUES(?,?)''',(moduleName, version,))
        self.conn.commit()
        
    def dropModules(self):
        c = self.conn.cursor()
        c.execute('DELETE FROM module')
    
    def checkIfModuleExist(self, moduleName):
        c = self.conn.cursor()
        c.execute('''SELECT * from module where nom = ?''', (moduleName,))
        result = []
        for element in c:
            result.append(element)
        return result 

    def userConnected(self, email):
        c = self.conn.cursor()
        c.execute(' UPDATE users SET connected = 1 WHERE email = "'+ email +'" ')
        c.execute(' SELECT id FROM users WHERE email = "'+ email +'" ')
        val = c.fetchone()
        return val
    
    def getMessagesEnvoye(self,idUser):
        c = self.conn.cursor()
        idMessage = c.execute('''SELECT message_id FROM messages_users WHERE poster_id = ?''', idUser)
        result = []
        for element in idMessage:       
            c.execute('''SELECT message FROM messages WHERE id = ? ORDER BY date DESC''', element)
            result.append(element)
        return result
    
    def getMessagesRecu(self, idUser):
        c = self.conn.cursor()
        idMessage = c.execute('''SELECT message_id FROM messages_users WHERE receiver_id = ? ''', idUser)
        result = []
        for element in idMessage:
            c.execute('''SELECT message FROM messages WHERE id = ? ORDER BY date DESC ''', element)
            result.append(element)
        return result
        
    def insertMessage(self,idUser,idReceiver, message):
        c = self.conn.cursor()
        c.execute('''INSERT INTO messages(message,date) VALUES(?,?)''',(message,datetime.now(),))
        idMessage = c.lastrowid
        c.execute('''INSERT INTO messages_users(message_id,poster_id,receiver_id) VALUES(?,?,?)''', (idMessage, idUser, idReceiver,))
        self.conn.commit()
        return 1
    
    # Alex
    def getMessageByID(self,id):
        c = self.conn.cursor()
        c.execute('SELECT * from messages WHERE id = ?',(id,))   
        check = c.fetchone()
        return check
    
    # Alex
    def getAllConvosUsers(self,id):
        c = self.conn.cursor()
        c.execute('SELECT poster_id, receiver_id from messages_users WHERE poster_id = ? OR receiver_id = ? ORDER BY message_id DESC',(id,id,))   
        result = []
        for element in c:
            result.append(element)
        return result 
    
    # Alex
    def getConvo(self,myId,userID):
        c = self.conn.cursor()
        c.execute('SELECT message_id,poster_id from messages_users WHERE (poster_id = ? AND receiver_id = ?) OR (poster_id = ? AND receiver_id = ?) ORDER By message_id DESC LIMIT 20',(myId,userID,userID,myId,))   
        result = []
        for element in c:
            result.append(element)
        return result 
    
    # Alex
    def getNextMessages(self,myId,userID,messageID):
        c = self.conn.cursor()
        c.execute('SELECT message_id,poster_id from messages_users WHERE message_id < ? AND ((poster_id = ? AND receiver_id = ?) OR (poster_id = ? AND receiver_id = ?)) ORDER By message_id DESC LIMIT 20',(messageID,myId,userID,userID,myId,))   
        result = []
        for element in c:
            result.append(element)
        return result 
        pass
    
    # Alex
    def getLastMessageID(self,myId,userID):
        c = self.conn.cursor()
        c.execute('SELECT message_id from messages_users WHERE (poster_id = ? AND receiver_id = ?) OR (poster_id = ? AND receiver_id = ?) ORDER By message_id DESC LIMIT 1',(myId,userID,userID,myId,))   
        val = c.fetchone()
        return val
    
    def getMessage(self):
        c = self.conn.cursor()
        c.execute('SELECT * from messages')   
        result = []
        for element in c:
            result.append(element)
        return result 
            
    
    def getMessagesId(self, idUser, idReceiver):
        c = self.conn.cursor()
        messages = c.execute('''SELECT message_id FROM messages_users WHERE poster_id = ? OR poster_id = ? AND receiver_id = ? OR receiver_id = ?''',idUser,idReceiver,idUser,idReceiver)
        result = []
        for element in messages:
            result.append(element)
        return element
    
    
    def insertPathImage(self, pathImage,idUser):
        c = self.conn.cursor()
        c.execute('''UPDATE users SET pathImage = ? WHERE id = ? ''', (pathImage, idUser))
        self.conn.commit()
        
    def getPathImage(self, idUser):
        c = self.conn.cursor()
        c.execute('''SELECT pathImage FROM users WHERE id = ? ''', (idUser, ))
        imagePath = c.fetchone()
        return imagePath
    
    
    def getCvID(self, idUser):
        c = self.conn.cursor()
        c.execute('SELECT cv FROM users WHERE id = ?', (idUser,))
        result = c.fetchone()
        return result
    
    def getCV(self, idCV):
        c = self.conn.cursor()
        c.execute('SELECT * FROM cv WHERE id = ?', (idCV,))
        result = c.fetchone()
        return result
        
    def getPorfolioID(self, idUser):
        c = self.conn.cursor()
        c.execute('SELECT porfolio FROM users WHERE id = ?', (idUser,))
        result = c.fetchone()
        return result
    
    def GetPorfolio(self, idPorfolio):
        c = self.conn.cursor()
        c.execute('SELECT * FROM porfolio WHERE id = ?', (idPorfolio,))
        result = c.fetchone()
        return result
    
    def closeDB(self):
        self.conn.close()
        
    #************************************************************ Method Alex **************************************************
    def insertInteret(self, interet):
        c = self.conn.cursor()
        c.execute('INSERT INTO interet(nom) VALUES(?)',(interet,))
        self.conn.commit()
        
    def getInterets(self):
        c = self.conn.cursor()
        c.execute('SELECT id,nom from interet')   
        result = []
        for element in c:
            result.append(element)
        return result 
    
    def getInteretsID(self):
        c = self.conn.cursor()
        c.execute('SELECT id from interet')   
        result = []
        for element in c:
            result.append(element)
        return result 
    
    def getUsers(self):
        c = self.conn.cursor()
        c.execute('SELECT id from users')   
        result = []
        for element in c:
            result.append(element)
        return result 
    
    def getLastUserID(self):
        c = self.conn.cursor()
        c.execute('SELECT id FROM users ORDER BY id DESC LIMIT 1')    
        check = c.fetchone()
        return check
    
    def getInteretUser(self,id):
        c = self.conn.cursor()
        c.execute('SELECT nom FROM interet WHERE id IN (SELECT interet_id FROM user_interet WHERE user_id = ?)',(id,))   
        result = []
        for element in c:
            result.append(element)
        return result 
    
    #Pourquoi ca retourne le nom si c'est ecrit ID SVP AIDEZ MOI
    def getInteretID(self,id):
        c = self.conn.cursor()
        c.execute('SELECT nom FROM interet WHERE id = ?)',(id,))   
        check = c.fetchone()
        return check 
    
    #def getInterestID(self, id):
    #    c = self.conn.cursor()
    #    c.execute('SELECT interet_id FROM user_interet WHERE user_id = ?',(id,))   
    #    result = []
    #    for element in c:
    #        result.append(element)
    #    return result 
    
    def linkUserInteret(self,userId,interetID):
        c = self.conn.cursor()
        c.execute('INSERT INTO user_interet(user_id, interet_id) VALUES(?,?)',(userId,interetID))
        self.conn.commit()
        
    def getFollowsByID(self,id):
        c = self.conn.cursor()
        c.execute('SELECT followed_id FROM follow WHERE id_user = ?',(id,))   
        result = []
        for element in c:
            result.append(element)
        return result 
    
    def getNoneFollowBasedOnInterest(self, id, qty):
        c = self.conn.cursor()
        c.execute('SELECT user_id FROM user_interet WHERE interet_id IN (SELECT interet_id FROM user_interet WHERE user_id = ?) and user_id NOT IN (SELECT followed_id FROM follow WHERE id_user = ?) LIMIT ?', (id,id, qty))
        result = []
        for element in c:
            result.append(element)
        return result
    
    def getNoneFollowBasedOnExpertise(self,id,qty):
        c = self.conn.cursor()
        c.execute('SELECT id FROM users WHERE id_expertise = (SELECT id_expertise FROM users WHERE id = ?) and id NOT IN (SELECT followed_id FROM follow WHERE id_user = ?) LIMIT ?', (id,id, qty))
        result = []
        for element in c:
            result.append(element)
        return result
    
    def getNoneFollow(self,id,qty):
        c = self.conn.cursor()
        c.execute('SELECT id FROM users WHERE id NOT IN (SELECT followed_id FROM follow WHERE id_user = ?) LIMIT ?' ,( id,qty,))   
        result = []
        for element in c:
            result.append(element)
        return result 
    
    def follow(self,followerID,followedID):
        c = self.conn.cursor()
        c.execute('INSERT INTO follow(id_user,followed_id) VALUES(?,?)',(followerID,followedID,))
        self.conn.commit()
        return "Follow"
    
    def unfollow(self,followerID,followedID):
        c = self.conn.cursor()
        c.execute('DELETE FROM follow WHERE id_user = ? AND followed_id = ?',(followerID,followedID,))
        self.conn.commit()
        return "Unfollow"
    
    def getAllUserId(self):
        c = self.conn.cursor()
        c.execute('SELECT id from users')
        result = []
        for element in c:
            result.append(element)
        return result  
    
    def getUsersIDByName(self,name):
        c = self.conn.cursor()
        c.execute("SELECT id from users WHERE nom LIKE '%"+name+"%' OR prenom LIKE '%"+name+"%'")
        result = []
        for element in c:
            result.append(element)
        return result  
    
    
    def updateUserInfos(self, userId, newPrenom, newNom, newEmail, newStatus, newDomaine, newInterest):
        c = self.conn.cursor()
        domaineChange = self.getExpertiseID(newDomaine)
        print(domaineChange)
        
        c.execute('''UPDATE users SET prenom = ?, nom = ?, email = ?, status = ?, id_expertise = ? WHERE id = ? ''', (newPrenom,newNom,newEmail,newStatus,str(domaineChange[0]),str(userId),))

        
        c.execute('''DELETE FROM user_interet WHERE user_id = ? ''', (userId,))

        
        for interet in newInterest:
            print(interet)
            c.execute('''INSERT INTO user_interet(user_id,interet_id) VALUES(?,?)''',(userId,interet,))

        self.conn.commit()
        
if __name__=="__main__":
    c=BD()
    print("End FaceMaster")