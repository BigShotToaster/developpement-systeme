#!/usr/bin/env python
# -*- coding: utf-8 -*-

import tkinter as tk
from tkinter import messagebox , OptionMenu

def mainInscriptionSignIn(root, listeExpertise, listeInteret, listeEtude):
    if root == None:
        root = tk.Tk()
        root.config(bg = COLOR_RED)
        root.title('Inscription')
        print("New Root")
    controller = InscriptionController(root, None, listeExpertise, listeInteret, listeEtude)
    return controller
COLOR_RED = "#e05841"
########   
####CONTROLER
########
class InscriptionController(object):
    def inscription(self):
        nom,prenom,email,password = self.view.infoperso.getNom(), self.view.infoperso.getPrenom(), self.view.infocon.getEmail(), self.view.infocon.getPass()
        idUser = -1
        cond = False
        if(self.bd != None and password != -1):
            idUser = self.bd.insertUser(prenom,nom,password,email)
        if(idUser != -1):
            cond = True
        self.view.inscriptionStatus(cond)
              
    def annuler(self):
        if(self.view.cancelConf()):
            self.root.destroy()

    def __init__(self,root, bd,  listeExpertise, listeInteret, listeEtude):
        listeP = None
        self.view = InscriptionView( root,  listeExpertise, listeInteret, listeEtude)
        self.root = root
        self.bd = bd
        self.view.buttonInsc["command"] = self.inscription
        self.view.buttonAnn["command"] = self.annuler
    def startGUI(self):
        self.root.mainloop()
    def getView(self):
        return self.view
        
########
##view
######

class InscriptionView(tk.Frame):   
    def inscriptionStatus(self, cond):
        if(cond):
            messagebox.showinfo("Inscription Reussie", "Inscription Reussie, l'Utilisateur a ete cree")
        else:
            messagebox.showerror("Inscription Echouee", "L'inscription n'a pas pu etre completee")
            
    def cancelConf(self):
        return messagebox.askyesno("Annuler", "Etes vous sure de vouloir annuler")       

    def createWidgets(self, listeExpertise=None, listeInteret=None, listeEtude= None):
        tk.Frame.grid(self)
        self.config(bg=COLOR_RED)
        self.grid(padx=20, pady=20)        
        self.lab = tk.Label(self, text = " Inscription", font = "GENERALFONT 19 bold", fg = 'white', bg=COLOR_RED)
        self.lab.grid(row=0, column=0,columnspan=1)
        self.infoperso = InfoPerso(self)
        self.infopro = InfoPro(self, listeExpertise, listeExpertise, listeEtude)
        #self.infopro.setList(listePro)
        self.infocon = InfoConnexion(self)
        self.infoperso.grid(row=2, column=0)
        self.infopro.grid(row=3, column=0)
        self.infocon.grid(row=4, column=0)        
        self.buttonInsc =tk.Button(self, text="S'inscrire", width=10, font = "GENERALFONT 12 bold", fg = 'white', bg=COLOR_RED)
        self.buttonInsc.grid(row=6, column=0)        
        self.buttonAnn = tk.Button(self, text="Annuler", width=10, font = "GENERALFONT 12 bold", fg = 'white', bg=COLOR_RED)
        self.buttonAnn.grid(row=7, column=0)
        
    def __init__(self, master,  listeExpertise=None, listeInteret=None, listeEtude=None):
        tk.Frame.__init__(self, master)    
        self.createWidgets( listeExpertise, listeInteret, listeEtude)

class LabEntryIns(tk.Frame):
    def __init__(self,master=None, texte="Texte", sizey=20):
        tk.Frame.__init__(self, master)
        tk.Frame.grid(self)
        self.grid(padx=20, pady=20)
        self.setupWid(texte, sizey)
        
    def grabText(self):
        self.ent.get()
    def asPassword(self, cond):
        if(cond):
            self.ent.config(show="*")
        else:
            self.ent.config(show="")
        
    def setupWid(self, texte, sizey):
        self.lab = tk.Label(self, text=texte, width=sizey, font = "GENERALFONT 12 bold", fg = 'white', bg=COLOR_RED)
        self.lab.grid(row=0, column=0,columnspan=2)
        self.ent = tk.Entry(self, width=sizey, font = "GENERALFONT 12 bold", fg = 'white', bg=COLOR_RED)
        self.ent.grid(row=0,column=3)
        self.ent.config(bg=COLOR_RED)

class CategorieInfo(tk.Frame):
    def __init__(self, master=None, texte=""):
        tk.Frame.__init__(self,master)
        tk.Frame.grid(self)
        self.grid(padx=20, pady=20)
        self.title = tk.Label(self, text=texte, font = "GENERALFONT 17 bold", fg = 'white', bg=COLOR_RED)
        self.title.grid(row=0, column=0, columnspan=1)
        self.setupWid()
        self.config(bg=COLOR_RED)
    
    def setupWid(self):
        pass
           
class InfoPerso(CategorieInfo):
    def __init__(self, master=None):
        CategorieInfo.__init__(self,master, "Informations Personnelles")
        
    def setupWid(self):
        self.nom = LabEntryIns(self,"Nom : ")
        self.prenom = LabEntryIns(self, "Prenom : ")                
        self.nom.grid(row=1,column=0)
        self.prenom.grid(row=3,column=0)
    def getPrenom(self):
        return self.nom.grabText()
    def getNom(self):
        return self.prenom.grabText()

class InfoConnexion(CategorieInfo):
    def __init__(self, master=None):
        CategorieInfo.__init__(self,master, "Informations de Connexion")
    
    def setupWid(self):
        self.email = LabEntryIns(self, "Email : ")
        self.password = LabEntryIns(self, "Password : ")
        self.passwordConf = LabEntryIns(self, "Password : ")
        self.password.asPassword(True)
        self.passwordConf.asPassword(True)
        
    def getPass(self):
        if self.passwordConf.grabText() == self.password.grabText():
            return self.password.grabText()
        else:
            return -1
    
    def getEmail(self):
        return self.email.grabText()
class LabOption(tk.Frame):
    def __init__(self,master=None, texte="Texte", optionList=None, sizey=20):
        tk.Frame.__init__(self, master)
        tk.Frame.grid(self)
        self.grid(padx=20, pady=20)
        self.defaultList = ["ERROR", "DEFAULT"]
        self.setupWid(texte, optionList,sizey)
        self.option.configure(width=sizey)
        
    def setupWid(self, texte, optionList, sizey):
        self.setOptionList(optionList)
        self.lab = tk.Label(self, text=texte, width=sizey, font = "GENERALFONT 12 bold", fg = 'white', bg=COLOR_RED)
        self.lab.grid(row=0, column=0,columnspan=2)  
        tmpOption = tk.StringVar(self)
        tmpOption.set(self.optionList[0])
        self.option = OptionMenu(self,tmpOption, *self.optionList, command=self.getSelected)
        self.option.config(font = "GENERALFONT 12 bold", fg = 'white', bg=COLOR_RED)
        self.option.grid(row=0,column=3)
        self.config(bg=COLOR_RED)
        
    def setOptionList(self, optionList=None):
        if(optionList == None):
            self.optionList = self.defaultList.copy()
            print("default")
            print(self.optionList)
        else:
            print("non None")
            self.optionList = optionList
    
    def getSelected(self, val):
        return val
    def getDefaultList(self):
        return self.defaultList

class InfoPro(CategorieInfo):
    def __init__(self, master, expertiseList=None,interestList= None, etudeListe=None ):
        self.expertiseList, self.interestList, self.etudeListe = expertiseList,interestList, etudeListe
        CategorieInfo.__init__(self,master, "Informations Professionnelles")
        
    def setInterestList(self, interestList):
        self.interestList = interestList
    def setExpertiseList(self, expertiseList):
        self.expertiseList = expertiseList
             
    def setupWid(self):        
        self.interets = LabOption(self,"Interets : ", self.interestList)
        self.expertise = LabOption(self,"Expertise : ", self.expertiseList)
        self.etude = LabOption(self, "Etude : ", self.etudeListe)
        self.interets.grid(row=1,column=0)
        self.expertise.grid(row=2,column=0)
        self.etude.grid(row=3,column=0)
    
#mainInscriptionSignIn(None, ["BBB", "bbvb", "ccc"], ["BBB", "bbvb", "ccc"], ["BBB", "bbvb", "ccc"]).startGUI()
