## -*- coding: UTF-8 -*- 

#Constante Global
#----------------
#DISPLAY SETTINGS
SCREEN_DIM_X = 700        #Largeur total de l'�cran
SCREEN_DIM_Y = 520          #Hauteur total de l'�cran
CANVAS_DIM_X = SCREEN_DIM_X/2.25          #Largeur de la zone de jeu
CANVAS_DIM_Y = SCREEN_DIM_Y*.8          #Hauteur de la zone de jeu

GENERALFONT = "helvetica"

GENERALFONT = "helvetica"

COLOR_WHITE = "#efefea"
COLOR_RED = "#e05841"
COLOR_LIGHRED = "#e26a56"

COLOR_TXTACTIVE = "#6d6d6d"
COLOR_TXT = "#999999"

COLOR_BLACK = "grey25" #"#353330"
COLOR_GREEN = "#97cb52"
COLOR_YELLOW = "#f9b450"
