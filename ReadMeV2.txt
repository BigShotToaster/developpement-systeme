Spicy Plaza
Version 2.0
=========================================================================

�QUIPE
------
- Alexandre Gauthier
- Hugo Turgeon-Nozaki
- Joey Dallaire
- Rapha�l Paquette-Agyemang
- William Frazilien
=========================================================================

G�N�RAL
-------
Un syst�me de messagerie est maintenant disponible vous permettant de communiquer entre usager
=========================================================================

FEATURES
--------
Client:
	- Aucun changement depuis la premi�re version.

	Version 1:
	- Le Client permet une connection par email et mot de passe (afin de tester, un utilisateur de d�veloppement a �t� cr�er. Email: test ; Mot de passe: 123 )
	- Un lien ouvre une petite fen�tre afin de cr�er un utilisateur simple
	- Une v�rification est effectu�e afin de nous assurer qu'un utilisateur n'existe pas d�j� avant de compl�ter une inscription


Serveur:
	- Un algorithme simple permet de retourner tous les messages faisant partie d'une conversation entre 2 usagers
	
	Version 1:
	- Le serveur utilise une base de donn�e sqlite afin de stocker des donn�es
	- Un algorithme simple permet de retourner les derni�res publications reli�es aux Int�r�ts et Follows de l'utilisateur
	- Un petit syst�me de g�n�ration de contenu al�atoire a �t� impl�ment� � des fins de d�veloppement. La variable bool�enne GENERATECONTENT dans le constructeur du server permet d'activer ou de d�sactiver cette g�n�ration.

Modules:
	Vitrine:
		- Changement du texte du bouton "refresh" par un logo
		- Ajout d'un bouton messagerie, en haut � droite, permettant d'ouvrir la messagerie
		- Dans la visualisation des profils des autres usagers � la droite de la fen�tre, un bouton Contacter est maintenant
		disponible. Lorsque l'on clique sur ce bouton, la messagerie s'ouvre avec comme conversation courante la conversation
		avec cet usager. Si aucune conversation existe, commence une nouvelle conversation.
		
		Version 1:
		- La vitrine est le fil d'actualit� et le centre de l'interface usag�
		- L'espace gauche de la fen�tre permet de visualiser rapidement les informations de son profil
		- En cliquant sur le nom d'un usager, nous pouvons voir son profil dans l'espace droit de la fen�tre
		- L'espace central affiche les derni�res publications int�ressantes � l'usager
		- Deux options de recherche en haut de la fen�tre permettent de visualiser du contenu uniquement reli� � ces choix
		- Un bouton refresh permet de rafraichir la Vitrine
		- Un bouton publier permet de faire des publications contenant du texte et un sujet
		- Un bouton Follow permet de s'abonner/d�sabonner � un utilisateur, priorisant ses publications
		
	Messagerie:
		- L'espace droit de la messagerie affiche la conversation pr�sentement s�lectionn�e.
		- Au bas de cet espace, il y a un bouton permettant d'envoyer le message contenu dans le champs texte � sa gauche.
		- L'espace gauche de la messagerie permet de s�lectionn� une conversation existante.
		- Dans le haut de l'espace gauche, il y a une barre de recherche qui permet de chercher dans les conversations.
		- La barre de recherche cherche autant les usagers avec lesquels on a d�j� une conversation (affich� en blanc)
		que les usager avec lesquels on n'a jamais eu de conversation (affich� en gris)
		- Lorsque l'on clique sur une conversation en gris, commence automatiquement une nouvelle conversation.
		- En haut � droite, il y a un bouton permettant d'ouvrir la vitrine. Ce bouton ne v�rifie cependant pas si la vitrine est d�j� d�marr�e.
		- Tout comme dans la vitrine, il y a un bouton refresh qui permet de forcer un refresh de la messagerie.
		- Ce refresh efface �galement les conversations d�marr�es mais vide et ram�ne � la derni�re conversation non vide
		- Lorsque la scrollbar se situe au bas de la page, il y a un refresh � toutes les secondes v�rifiant la pr�sence de nouveaux messages.
		- Lorsque le chariot atteint le haut de la scrollbar, les messages plus anciens sont aussi t�l�charg�s et la taille du scroll et du canevas change.

=========================================================================

CHANGEMENTS FUTUR
-----------------
- V�rifier si l'utilisateur est d�j� connect� afin de ne permettre qu'une seule connection � la fois � un compte
- Int�grer une confirmation de mot de passe lors de l'inscription
- Int�grer diff�rents messages afin d'avertir d'une erreur lors de mauvaises saisies d'informations lors de l'inscription
- Am�liorer l'algorithme de retour de publication
- Int�grer un syst�me d'upload de photo de profil et de t�l�chargement de ces photos � partir du server
- Int�grer une confirmation de Unfollow dans la Vitrine
- Int�grer un syst�me de Rating dans le fil d'actualit�. Remplacer les num�ros de ID dans l'affichage de la publication par un visuel reli� au Rating
- V�rification de la pr�sence d'une vitrine ouverte pour ne pas en ouvrir 2 en m�me temps.
