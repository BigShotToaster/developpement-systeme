## -*- coding: UTF-8 -*- 

from tkinter import *
from tkinter import messagebox
from settings import *
import re
from tkinter.ttk import Treeview

#For testing only
import random

class Vue():
    def __init__(self,parent):
        self.parent=parent
        self.root=Tk()
        self.root.title('Spicy Plaza - Log In')
        self.root.configure(background = COLOR_RED)
        self.root.geometry( str(SCREEN_DIM_X) + "x" + str(SCREEN_DIM_Y) )
        
        self.menuPanel = Frame(self.root)
        self.separateurInfos = Frame()
        self.iniMenu()
    
    def iniMenu(self):
        self.menuPanel.destroy()
        self.menuPanel = Frame(self.root)
        self.menuPanel.configure(background = COLOR_RED)
        self.menuPanel.pack(pady = 50)
          
        self.imgTitle = PhotoImage(file="SpicyPlaza_line_300x97.png")      
        self.title = Label(self.menuPanel, image = self.imgTitle)
        self.title.configure(background=COLOR_RED)
        self.title.grid(row = 0)
        
        self.separateurInfos = Frame(self.menuPanel)
        self.separateurInfos.configure(background = COLOR_RED)
        self.separateurInfos.grid(row = 1, pady = 30)
        
        
        self.emailLabel = Label(self.separateurInfos, text="Addresse courriel : ", font="Helvetica 18 bold italic", fg = 'white', anchor = W)
        self.emailLabel.configure(background=COLOR_RED, width = 15)         
        self.emailLabel.grid(row = 0, pady = 10)
        
        self.varEmail = StringVar(self.root)
        self.varEmail.set("test")
        self.email = Entry(self.separateurInfos,textvariable=self.varEmail)
        self.email.grid(row = 0, column = 1)
        
        self.passwordLabel = Label(self.separateurInfos, text="Mot de passe        : ", font="Helvetica 18 bold italic", fg = 'white', anchor = W)
        self.passwordLabel.configure(background=COLOR_RED, width = 15)         
        self.passwordLabel.grid(row = 1, pady = 10)
        
        self.varPwd = StringVar(self.root)
        self.varPwd.set("123")
        self.password = Entry(self.separateurInfos,textvariable=self.varPwd)
        self.password.config(show="*")
        self.password.grid(row = 1, column = 1)
        
        self.separateurGo = Frame(self.menuPanel)
        self.separateurGo.configure(background = COLOR_RED)
        self.separateurGo.grid(row = 3)
        
        self.flecheGauche = Label(self.separateurGo, text="--->>", font="GENERALFONT 22 bold", fg = 'black', anchor=S)
        self.flecheGauche.configure(background=COLOR_RED)
        #self.flecheGauche.grid(row = 0, column = 0)
        
        self.quitter = Button(self.separateurGo, text= "GO", font = "GENERALFONT 14 bold italic", relief=RIDGE,bd=0,fg=COLOR_TXTACTIVE,activeforeground=COLOR_BLACK,activebackground=COLOR_WHITE,bg="#ffffff",command = self.connexionUser)
        self.quitter.configure(width = 10, height = 1)
        self.quitter.grid(padx = 20, row = 0, column = 1)
        
        self.flecheDroite = Label(self.separateurGo, text="<<---", font="GENERALFONT 22 bold", fg = 'black', anchor=S)
        self.flecheDroite.configure(background=COLOR_RED)
        #self.flecheDroite.grid(row = 0, column = 2)
        
        self.separateurInscription = Frame(self.menuPanel)
        self.separateurInscription.configure(background = COLOR_RED)
        self.separateurInscription.grid(row = 4, pady = 40)
        
        self.inscriptionLabel = Label(self.separateurInscription, text="Pas de compte?", font="GENERALFONT 17 bold italic", fg = 'white', anchor = W)
        self.inscriptionLabel.configure(background=COLOR_RED, width = 30)         
        #self.inscriptionLabel.pack(pady = 10, padx = 10,side=LEFT)
        
        self.quitter = Button(self.separateurInscription, relief=FLAT, text= "Pas de compte? Inscrivez-vous!!", font = "GENERALFONT 14 bold italic underline",bd = 0,bg=COLOR_RED,activebackground=COLOR_RED,fg="#ffffff",command=self.parent.openSignIn)
        self.quitter.configure(height = 1)
        self.quitter.pack(padx = 10,side=LEFT)
        
        self.center(self.root)
    
    def messageUsager(self, message):
        messagebox.showinfo("Message!", message)
        
    def connexionUser(self):
        if (self.email.get() == "" or self.password.get() == ""):
            self.messageUsager("Un ou plusieurs champs ont été laissés vides")
        else:
            rep = self.parent.verificationConnexion(self.email.get(), self.password.get())
            if rep == -1 : 
                self.messageUsager("Aucun profil ne correspond à ces identifiants")
           
                
    def windowSignIn(self,expertises):
        self.windowSign = Toplevel(height=500, width=700)
        self.windowSign.title("Inscription")
        self.windowSign.configure(background = 'grey25')
        
        self.varInt = StringVar(self.root)
        self.varInt.set(list(expertises.keys())[0])
        
        self.mainFrame = Frame(self.windowSign)
        self.mainFrame.configure(pady = 15, padx = 30, background = COLOR_RED)
        self.mainFrame.pack()
        
        Label(self.mainFrame, text="Adresse Email ", font = "GENERALFONT 12 bold", fg = 'white', bg=COLOR_RED,anchor=W).grid(row=0,column=0)
        self.signInEmail = Text(self.mainFrame, height = 1, width = 30)
        self.signInEmail.grid(row=0,column=1)
        
        Label(self.mainFrame, text="Prénom ", font = "GENERALFONT 12 bold", fg = 'white', bg=COLOR_RED,anchor=W).grid(row=1,column=0)
        self.signInPrenom = Text(self.mainFrame, height = 1, width = 30)
        self.signInPrenom.grid(row=1,column=1)
        
        Label(self.mainFrame, text="Nom ", font = "GENERALFONT 12 bold", fg = 'white', bg=COLOR_RED,anchor=W).grid(row=2,column=0)
        self.signInNom = Text(self.mainFrame, height = 1, width = 30)
        self.signInNom.grid(row=2,column=1)
        
        Label(self.mainFrame, text="Password ", font = "GENERALFONT 12 bold", fg = 'white', bg=COLOR_RED,anchor=W).grid(row=3,column=0)
        self.signInPwd = Text(self.mainFrame, height = 1, width = 30)
        self.signInPwd.grid(pady=10,row=3,column=1)
        
        label = Label(self.mainFrame, text="Domaine d'Expertise : ", font = "GENERALFONT 12 bold", fg = 'white', bg=COLOR_RED)
        label.grid(row = 4,column = 0)
        
        sujetMenu = OptionMenu(self.mainFrame, self.varInt, *expertises)
        sujetMenu.configure(font = "GENERALFONT 12 bold",relief=FLAT,bd = 0, fg=COLOR_TXT,activeforeground=COLOR_TXTACTIVE,bg="#ffffff",activebackground="#ffffff",anchor=W)
        sujetMenu.grid(row = 4,column = 1)
        
        b = Button(self.mainFrame, text="S'Inscrire", font = "GENERALFONT 14", anchor = E, command = self.signIn)
        b.grid(row=5,column=0)
        
        #Appel pour centrer la fenÃªtre
        self.center(self.windowSign)
        
    def signIn(self):
        email = self.signInEmail.get(1.0, 'end-1c')
        prenom = self.signInPrenom.get(1.0, 'end-1c')
        nom = self.signInNom.get(1.0, 'end-1c')
        pwd = self.signInPwd.get(1.0, 'end-1c')
        expertise = self.varInt.get()
            
        if email != "" and prenom != "" and nom != "" and pwd != "":
            self.parent.signIn(email,prenom,nom,pwd,expertise)
            
            
    def closeSignIn(self):
        self.varEmail.set(self.signInEmail.get(1.0, 'end-1c'))
        self.varPwd.set(self.signInPwd.get(1.0, 'end-1c'))
        self.windowSign.destroy()
        
    

    def center(self,win):
        """
        centers a tkinter window
        :param win: the root or Toplevel window to center
        """
        win.update_idletasks()
        width = win.winfo_width()
        frm_width = win.winfo_rootx() - win.winfo_x()
        win_width = width + 2 * frm_width
        height = win.winfo_height()
        titlebar_height = win.winfo_rooty() - win.winfo_y()
        win_height = height + titlebar_height + frm_width
        x = win.winfo_screenwidth() // 2 - win_width // 2
        y = win.winfo_screenheight() // 2 - win_height // 2
        win.geometry('{}x{}+{}+{}'.format(width, height, x, y))
        win.deiconify()

        
                