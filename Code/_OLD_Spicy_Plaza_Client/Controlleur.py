## -*- coding: UTF-8 -*- 

import xmlrpc.client
from settings import *
import os,os.path
import sys
import socket
from subprocess import Popen 

from Vue import *
from Modele import * 


class Controleur():
    def __init__(self):
        self.modele= Modele(self) 
        self.vue= Vue(self)
        self.server=None
        self.id = None
        self.vue.root.mainloop()
        
    
    def loginclient(self,ip="127.0.0.1",port="50017"):
        ad="http://"+ip+":"+port
        self.server=xmlrpc.client.ServerProxy(ad)
        print(self.server) 
        
    def openSignIn(self):
        self.loginclient()
        self.getInterests()
        self.getExpertises()
        self.vue.windowSignIn(self.modele.expertises)
        
    def getInterests(self):
        request = self.server.getInterests()
        if request != -1:
            for sujet in request:
                self.modele.addInterests(sujet[1],sujet[0])
                
    def getExpertises(self):
        request = self.server.getExpertises()
        if request != -1:
            for sujet in request:
                self.modele.addExpertise(sujet[1],sujet[0])
                
    def signIn(self,email,prenom,nom,pwd,expertise):
        expertiseId = self.modele.expertises[expertise]
        
        
        req = self.server.addUser(prenom,nom,pwd,email,expertiseId)
        
        if req == 1:
            self.vue.closeSignIn()
        elif req == -1:
            self.vue.messageUsager("Email déjà utilisé")
        
    def verificationConnexion(self, email, password):
        self.loginclient()
        verif = self.server.verificationConnexion(email, password)
        print(verif)
        if verif == -1:
            return verif #afficher message erreur dans vue
        else :
            self.id = verif
            self.ouvertureModuleVitrine()
            self.server.connect(self.id)
            return 0
            #Appel du module vitrine
    
    def ouvertureModuleVitrine(self):
        repRequeteModule = self.server.requetemodule("vitrine")
        #print(repRequeteModule)
        if repRequeteModule:
            #print(repRequeteModule[0])
            cwd=os.getcwd()
            lieuApp="\\"+repRequeteModule[0]
            lieu=cwd+lieuApp
            #print(lieu)
            if not os.path.exists(lieu):
                os.mkdir(lieu) #plante s'il exist deja
            bonPath=repRequeteModule[1]
            #print(repRequeteModule[1])
            #print("contenu dossier/fichier", repRequeteModule[2])
            for i in repRequeteModule[2]:
                #print(i)
                if i[0]=="fichier":
                    nom=bonPath+i[1]
                    #print("fichier", nom)
                    rep=self.server.requetefichier(nom)
                    fiche=open(lieu+"\\"+i[1],"wb")
                    fiche.write(rep.data)
                    fiche.close()
                elif i[0]=="dossier":
                    if not os.path.exists(lieu+"\\"+i[1]):
                        os.mkdir(lieu+"\\"+i[1])
                elif i[0]=="sous-fichier":
                    nom=bonPath+i[1]+'\\'+i[2]
                    #print("sous-fichier", nom)
                    rep=self.server.requetefichier(nom)
                    fiche=open(lieu+"\\"+i[1]+"\\"+i[2],"wb")
                    fiche.write(rep.data)
                    fiche.close()
            chaineappli=lieu+lieuApp+".py"

        
            self.pid = Popen([sys.executable, chaineappli,str(self.id)],shell=0) 
        else:
            print("RIEN")

if __name__ == '__main__':
    c=Controleur()