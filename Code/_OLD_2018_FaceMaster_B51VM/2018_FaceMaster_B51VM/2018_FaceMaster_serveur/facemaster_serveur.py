# -*- encoding: utf-8 -*-

#import Pyro4
from xmlrpc.server import SimpleXMLRPCServer

import xmlrpc.client

import os,os.path
from threading import Timer
import sys
import socket
import time
import random
import re

import BD
import shutil



s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect(("gmail.com",80))
monip=s.getsockname()[0]
print("MON IP SERVEUR",monip)
s.close()

#daemon = Pyro4.core.Daemon(host=monip,port=9999) 
daemon= SimpleXMLRPCServer(("127.0.0.1",50017))


            
class ControleurServeur(object):
    def __init__(self):
        GENERATECONTENT = False #Boolean pour générer du contenu
        
        rand=random.randrange(1000)+1000
        #self.checkping=0
        self.delaitimeout=25   # delai de 5 secondes
        
        self.database = BD.BD(GENERATECONTENT)
        self.bd = self.database
        
        self.checkModule()
        
        if GENERATECONTENT:
            self.createRandomContent()
        
    def checkModule(self):
        self.bd.dropModules()
        
        modules = next(os.walk('./fm_modules'))[1]
        
        for module in modules:
            with open('./fm_modules/'+module+'/readme.txt') as f:
                version = f.readline()
            self.bd.insertModule(module,version)
    
    def verificationConnexion(self, email, password):
        rep = self.database.getIdentifiers(email, password)
         
        if rep == None:
            return -1 #les identifiants ne correspondent a aucun profil dans la base de donnée
        else :
            val = self.database.userConnected(email)
            return val[0]
        
    def addUser(self, prenom, nom,password, email,expertise):
        req = self.bd.getUserByEmail(email)
        if req:
            return -1
        else:
            print(expertise)
            self.database.insertUser(prenom, nom, password, email,expertise)
            return 1
    
    def connect(self,id):
        self.bd.connectUser(id)
        return 1
    
    def disconnect(self,id):
        self.bd.disconnectUser(id)
        return 1
        
    def testPyro(self):
        return 42
        
    def loginauserveur(self,nom):
        rep=self.modele.creerclient(nom)
        return rep

    def ajoutModule(self, name, version):
        listOfModule = self.database.checkIfModuleExist(name)
        if(len(listOfModule) == 0):
            self.database.insertModule(name, version)
    
    def requetemodule(self,mod):
        if len(self.database.checkIfModuleExist(mod)) != 0:
            cwd=os.getcwd()
            #print(mod,os.getcwd())
            if os.path.exists(cwd+"\\fm_modules\\"):
                dirmod=cwd+"\\fm_modules\\"+mod+"\\"
                if os.path.exists(dirmod):
                    print("TROUVE")
                    listefichiers=[]
                    for i in os.listdir(dirmod):
                        if os.path.isfile(dirmod+i):
                            val=["fichier",i]
                            listefichiers.append(val)
                        else:
                            val=["dossier",i]
                            listefichiers.append(val)
                            for sousDossier in os.listdir(dirmod+i):
                                if os.path.isfile(dirmod+i+'\\'+sousDossier):
                                    val=["sous-fichier",i,sousDossier]
                                    listefichiers.append(val)
                            
                    return [mod,dirmod,listefichiers]
                return "PATH DOESN'T EXIST - dirmod"
            return "PATH DOESN'T EXIST - fm_modules"
        return "checkIfModuleExist return 0"
            
            
            
    def requetefichier(self,lieu):
        fiche=open(lieu,"rb")
        contenub=fiche.read()
        fiche.close()
        return xmlrpc.client.Binary(contenub)
            
    
    def verifiecontinuation(self):
        t=int(time.time())
        if (t-self.checkping) > self.delaitimeout: 
            self.fermer()
        else:
            tim=Timer(1,self.verifiecontinuation)
            tim.start()
        
    def quitter(self):
        t=Timer(1,self.fermer)
        t.start()
        return "ferme"
    
    def jequitte(self,nom):
        del self.modele.clients[nom]
        del self.modele.cadreDelta[nom]
        if not self.modele.clients:
            self.quitter()
        return 1
    
    def fermer(self):
        print("FERMETURE DU SERVEUR")
        daemon.shutdown()
        
        
        
        
    # ************************************* Random Gen Method *********************************************
    def createInterets(self):
        sujets = ["None","Informatique","Jeux-Vidéo","Sport","Anime","Histoire","Art","Voyage"]
        for sujet in sujets:
            self.bd.insertInteret(sujet)
            
    def createExpertise(self):
        expertises = ["Designer","Informaticien","Programmer","Entrepreneur","Comptable"]
        for expertise in expertises:
            self.bd.insertExpertise(expertise)
            
    def genUser(self):
        prenoms = ["Thomas", "Livio", "Julian", "Walo" , "Abi", "Mario", "Luigi", "Zena", "Dylan", "Theo", "Charlie"]
        noms = ["Tinner", "Joss","Magaro","Russo","Rocco", "Jackson", "Turner", "Lynch", "Talley", "Woods", "Black"]
        
        nom = noms[random.randint(0,len(noms)-1)]
        prenom = prenoms[random.randint(0,len(prenoms)-1)]
        
        expertises = self.bd.getExpertises()
        expertise = expertises[random.randint(1,len(expertises)-1)][0]
        expertiseNom = self.bd.getExpertiseName(expertise)[0]
        
        self.bd.insertUser(prenom,nom,"123",prenom+nom+str(random.randint(0,9999))+"@email.com",expertise)
        userId = self.bd.getLastUserID()[0]
        
        
        interets = self.bd.getInteretsID()
        for i in range(2,random.randint(2,len(interets))):
            self.bd.linkUserInteret(userId,i)
        
        
    def genPost(self):
        message = ""
        
        for i in range(random.randint(15,len(self.words)-1)):
            message+=str(self.words[i])
            message+=" "
        
        sujets = self.bd.getInteretsID()
        sujet = sujets[random.randint(1,len(sujets)-1)][0]
        
        users = self.bd.getUsers()
        user = users[random.randint(0,len(users)-1)][0]
        
        
        self.bd.insertPublication(message, int(user), int(sujet))
        
    def createRandomContent(self):
        self.createInterets()
        self.createExpertise()
        self.bd.insertExpertise("Informatique")
        
        string = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sit amet elementum ipsum, eget dignissim sapien.\n Etiam eu nisl euismod, egestas odio eget, feugiat mi. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.\n Morbi vulputate interdum bibendum. Donec feugiat turpis id elit commodo, vitae volutpat massa sagittis. Nullam congue in risus ut congue. Nulla enim risus, laoreet quis porta in, malesuada et est. Duis eu risus vel dolor sagittis feugiat ac et ante.\n Maecenas porttitor lectus massa, et varius lacus semper et. Aliquam felis libero, blandit sit amet ullamcorper vel, ultricies vitae urna. Curabitur viverra libero ex, at sodales dui fermentum nec. Fusce maximus pellentesque magna et fermentum.Nam tempor mattis libero, non ultricies lacus.\n Sed mollis nisi sit amet velit interdum, ac pellentesque dui rutrum. Nunc dapibus congue mauris, vitae venenatis mi finibus sit amet. Ut malesuada lorem sed suscipit dictum. Vivamus ullamcorper purus urna, at vestibulum odio efficitur quis. Praesent hendrerit id odio sed egestas. Proin non feugiat ante.\n Nullam ut purus aliquet ante elementum bibendum ac mattis magna. Suspendisse potenti. Nulla eu eleifend ex. Nulla tincidunt diam at tempus ultricies. Sed gravida ex et dolor bibendum sodales. Vivamus porttitor facilisis mi. Aliquam et ligula eget tellus tristique efficitur eget nec leo."
        self.words = re.findall('\w+', string)
        
        self.bd.insertUser("Test","Hello","123","test",1)
        userId = self.bd.getLastUserID()[0]
        interets = self.bd.getInteretsID()
        for i in range(2,5):
            self.bd.linkUserInteret(userId,i)
            
        for i in range (10):
            self.genUser()
            
        for i in range (100):
            self.genPost()
            
            
    #******************************************** Vitrine Get Method **********************************
    
    def getInterests(self):
        return self.bd.getInterets()
    
    def getExpertises(self):
        return self.bd.getExpertises()
    
    def getUser(self,id):
        req = self.bd.getUser(id)
        if req:
            return req
        return -1
    
    def getInteretsByID(self,id):
        return self.bd.getInteretUser(id)
    
    def getPublications(self,userId,qty=10, sujet="None",email="None",id=-1):

        if sujet == "None" and email == "None":
            posts = self.bd.newGetPublications(id,qty,sujet,email,userId)
        else:
            posts = self.bd.newGetPublications(id,qty,sujet,email)
        
        return posts
    
    def getMyFollow(self,ID):
        req = self.bd.getFollowsByID(ID)
        if req:
            return req
        return -1
    
    def follow(self,followerID,followedID):
        req = self.bd.follow(followerID,followedID)
        if req=="Follow":
            return req
        
    def unfollow(self,followerID,followedID):
        req = self.bd.unfollow(followerID,followedID)
        if req=="Unfollow":
            return req

    def addPublication(self, message, userID, interetID):
        self.database.insertPublication(message,userID,interetID)
        return -1

controleurServeur=ControleurServeur()
daemon.register_instance(controleurServeur)  
 
print("Serveur XML-RPC actif")
daemon.serve_forever()

controleurServeur.addUser("Will","IAm", "Yikes","yikers.com",1)
#controleurServeur.ajoutModule("vitrine", 1)
#controleurServeur.requetemodule("fm_sql")