# -*- coding: utf-8 -*-
from SETTINGS import *
from threading import Timer

from Publication import *
from User import *

import re
import time
import random

class Modele():
    def __init__(self,parent):
        self.parent=parent
        self.publications = []
        self.users = {}
        self.searchOption = ["None","None"]
        self.interets = {}
        
    def addPost(self,post,user,interets):
        poster = User(self,user[0],user[3],user[2],user[1],user[4],interets)
        publication = Publication(self,post[0],user[0],post[1],post[5],post[3],post[4])
        self.users[user[0]] = poster
        self.publications.append(publication)
        
        return publication
    
    def addProfil(self,user,interets):
        myProfil = User(self,user[0],user[3],user[2],user[1],user[4],interets)
        self.users[user[0]] = myProfil
        
        return myProfil
    
    def addInterests(self,sujetNom,sujetID):
        self.interets[sujetNom] = sujetID
        
    def passSearch(self,sujet,email):
        self.searchOption = [sujet,email]
        
    def updateFollow(self,user):
        self.users[user].follow = True