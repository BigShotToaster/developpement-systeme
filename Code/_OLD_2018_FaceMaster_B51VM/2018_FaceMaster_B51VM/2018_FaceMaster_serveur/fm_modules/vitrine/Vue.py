## -*- coding: UTF-8 -*- 

from tkinter import *
from SETTINGS import *
import re
from tkinter.ttk import Treeview
from datetime import date, time, datetime

#For testing only
import random

class Vue():
    def __init__(self,parent):
        self.parent=parent
        self.root=Tk()
        self.root.title('Spicy Plaza - La Vitrine')
        self.root.configure(background=COLOR_WHITE)
        
        self.root.geometry(str(SCREEN_DIM_X)+"x"+str(SCREEN_DIM_Y))
        self.scrollY = 0
        self.printY = 0
        
        self.topPanel = Frame()
        self.centerCanvevas = Canvas()
        self.profilPanel = Frame()
        self.yscrollbar = Scrollbar()
        self.userPanel = Frame()
        self.centerFrame = Frame()
        self.searchPanel = Frame()
        
        self.tkvar = StringVar(self.root)
        self.tkpost = StringVar(self.root)

        
        self._userDic = {}
        
        self.root.bind("<MouseWheel>", self.OnMouseWheel)
        
    def iniModule(self,publications,users,myProfil,interets):
        self.centerCanvevas.destroy()
        self.topPanel.destroy()
        self.profilPanel.destroy()
        self.yscrollbar.destroy()
        self.userPanel.destroy()
        self.centerFrame.destroy()
        
        
        self.topPanel=Frame(self.root,width=SCREEN_DIM_X,height=50, bg = "#e05841")      
        self.topPanel.pack(fill = BOTH, side = TOP)
        
        self.img = PhotoImage(file="./vitrine/img/SpicyPlaza_line.png")  
        label = Label(self.topPanel,image=self.img,bg="#e05841")
        label.pack(side=LEFT,padx=60)
        
        self.refreshBtn = Button(self.topPanel,text="Refresh", font = "GENERALFONT 12 bold underline",relief=FLAT,bd = 0, bg="#e05841",fg="#ffffff",activebackground="#e05841",activeforeground="#e8e8e8",command=self.parent.refreshView)
        self.refreshBtn.pack(padx = 20,side=RIGHT,pady=10)
        
        
        self.profilPanel=Frame(self.root,width=SCREEN_DIM_X/4,height=SCREEN_DIM_Y,  bg=COLOR_WHITE)      
        self.profilPanel.pack(side = LEFT, pady=100,fill=BOTH)
        
        
        self.yscrollbar = Scrollbar(self.root)
        self.yscrollbar.pack(side=RIGHT,fill=Y)
        
        self.userPanel=Frame(self.root,width=SCREEN_DIM_X/4,height=SCREEN_DIM_Y,  bg=COLOR_WHITE)      
        self.userPanel.pack(side = RIGHT, pady=100,fill=BOTH)
        
        self.userPanelContent = Frame(self.userPanel,width=SCREEN_DIM_X/4+4,height=SCREEN_DIM_Y,bg=COLOR_WHITE)
        self.userPanelContent.pack(expand=TRUE,fill=BOTH)
        
        self.centerFrame=Frame(self.root)
        self.centerFrame.pack(side = TOP, fill = BOTH)
        
        self.searchPanel=Frame(self.centerFrame,bg=COLOR_WHITE,height = 80,highlightcolor=COLOR_WHITE,highlightthickness=5)
        self.searchPanel.pack(side = TOP,fill=BOTH)
        self.printSearch(interets)
       
        self.centerCanvevas=Canvas(self.centerFrame,height=SCREEN_DIM_Y,bg=COLOR_WHITE,bd=0,highlightthickness=0,yscrollcommand=self.yscrollbar.set,scrollregion=(0, 0, 0, self.scrollY))       
        self.centerCanvevas.pack(side = TOP, fill = BOTH)
        
        self.yscrollbar.config(command=self.centerCanvevas.yview)
        
        self.centerCanvevas.bind("<Button-1>",self.mousePress)
        
        self.printProfil(myProfil,interets)

        #self.printUser()
        
        self.updateView(publications,users)
        
        self.center(self.root)
        
    def printSearch(self,interets):
        self.tkvar.set(list(interets.keys())[0])
        sujetMenu = OptionMenu(self.searchPanel, self.tkvar, *interets)
        sujetMenu.configure(font = "GENERALFONT 12 bold",relief=FLAT,bd = 0, fg=COLOR_TXT,activeforeground=COLOR_TXTACTIVE,bg="#ffffff",activebackground="#ffffff")
        sujetMenu.pack(side=LEFT,padx=5,fill=BOTH)
        
        frameUser = Frame(self.searchPanel,bg="#ffffff")
        frameUser.pack(side=LEFT,fill=BOTH,expand=TRUE,padx=5)
        
        self.userSearch = Text(frameUser)
        self.userSearch.insert(1.0, "None")
        self.userSearch.configure(font = "GENERALFONT 12 bold",relief=FLAT,bd = 0, fg=COLOR_TXT,bg="#ffffff",height=1,width=40)
        self.userSearch.pack(side=LEFT,fill=X)
        
        goBtn = Button(self.searchPanel, text = "GO",command=self.searchOption)
        goBtn.configure(font = "GENERALFONT 12 bold",height=2,width=5,relief=FLAT,bd = 0, bg="#e05841",fg="#ffffff",activebackground="#e26a56",activeforeground="#e8e8e8")
        goBtn.pack(side=RIGHT,padx=5)
        
    def printProfil(self,myProfil,interets):
        self.avatar = PhotoImage(file="./vitrine/img/profil_small.png")  
        label = Label(self.profilPanel,image=self.avatar,width=SCREEN_DIM_X/4,height=210)
        label.pack()
        
        self.profilInsideFrame=Frame(self.profilPanel,width=SCREEN_DIM_X/4,bg = '#ffffff')
        self.profilInsideFrame.pack(padx=50,fill=BOTH)
        
        
        username = Label(self.profilInsideFrame,font="GENERALFONT 12 bold",text=myProfil.prenom+" "+myProfil.nom,bg='#ffffff',fg="#6d6d6d",anchor=W,justify=LEFT,wraplength=176)
        username.pack(pady=10,padx=10,fill=X)
        
        Label(self.profilInsideFrame,height=0,bg=COLOR_WHITE).pack(pady=0,fill=X)
        
        Label(self.profilInsideFrame,font="GENERALFONT 10",text=myProfil.email,bg='#ffffff',fg="#6d6d6d",anchor=W,justify=LEFT,wraplength=176).pack(pady=5,padx=10,fill=X)
        
        Label(self.profilInsideFrame,font="GENERALFONT 10",text="Status: "+myProfil.status,bg='#ffffff',fg="#6d6d6d",anchor=W,justify=LEFT,wraplength=176).pack(pady=5,padx=10,fill=X)
        
        Label(self.profilInsideFrame,font="GENERALFONT 10",text="Domaine: "+myProfil.expertise,bg='#ffffff',fg="#6d6d6d",anchor=W,justify=LEFT,wraplength=176).pack(pady=5,padx=10,fill=X)
        
        Label(self.profilInsideFrame,font="GENERALFONT 10",text="Intérets: ",bg='#ffffff',fg="#6d6d6d",anchor=W).pack(pady=5,padx=10,fill=X)
        for interet in myProfil.interets:
            Label(self.profilInsideFrame,font="GENERALFONT 9",text=interet,bg='#ffffff',fg="#6d6d6d",anchor=W).pack(pady=1,padx=20,fill=X)
            
        Label(self.profilInsideFrame,height=0,bg="#ffffff").pack(pady=0,fill=X)
        

        button1 = Button(self.profilPanel, text = "Publier", command = lambda:self.windowPublishPost(interets))
        button1.configure(font = "GENERALFONT 12 bold",relief=FLAT,bd = 0, bg="#e05841",fg="#ffffff",activebackground="#e26a56",activeforeground="#e8e8e8")
        button1.pack(padx=62,fill=X, pady=10)
            
        
    def printUser(self,profil):
        self.deleteUser()
        self.userPanelContent = Frame(self.userPanel,width=SCREEN_DIM_X/4+40,height=SCREEN_DIM_Y,bg=COLOR_WHITE)
        self.userPanelContent.pack(fill=BOTH)
        
        self.userAvatar = PhotoImage(file="./vitrine/img/profil_blue_small.png")  
        label = Label(self.userPanelContent,image=self.userAvatar,width=SCREEN_DIM_X/4,height=210)
        label.pack(side=TOP)
        
        if profil.follow:
            txtBtn = "UnFollow"
        else:
            txtBtn = "Follow"
        
        self.btnFollow = Button(self.userPanelContent, text = txtBtn,command=lambda:self.parent.followByID(profil.id))
        self.btnFollow.configure(font = "GENERALFONT 12 bold",relief=FLAT,bd = 0, bg="#e05841",fg="#ffffff",activebackground="#e26a56",activeforeground="#e8e8e8")
        self.btnFollow.pack(side=TOP, padx=62,fill=BOTH, pady=10)
        
        self.userInsideFrame=Frame(self.userPanelContent,width=SCREEN_DIM_X/4,bg = '#ffffff')
        self.userInsideFrame.pack(padx=50,expand=TRUE,fill=BOTH)
        
        
        Label(self.userInsideFrame,font="GENERALFONT 12 bold",text=profil.prenom+" "+profil.nom,bg='#ffffff',fg="#6d6d6d",anchor=W,justify=LEFT,wraplength=176).pack(pady=10,padx=10,fill=X)
        
        Label(self.userInsideFrame,height=0,bg=COLOR_WHITE).pack(pady=0,fill=X)
        
        Label(self.userInsideFrame,font="GENERALFONT 10",text=profil.email,bg='#ffffff',fg="#6d6d6d",anchor=W,justify=LEFT,wraplength=176).pack(pady=5,padx=10,fill=X)
        
        
        label = Label(self.userInsideFrame,font="GENERALFONT 10",text="Status: "+profil.status,bg='#ffffff',fg="#6d6d6d",anchor=W,justify=LEFT,wraplength=176).pack(pady=5,padx=10,fill=X)
     
        
        Label(self.userInsideFrame,font="GENERALFONT 10",text="Domaine: "+profil.expertise,bg='#ffffff',fg="#6d6d6d",anchor=W,justify=LEFT,wraplength=176).pack(pady=5,padx=10,fill=X)
        
        Label(self.userInsideFrame,font="GENERALFONT 10",text="Intérets: ",bg='#ffffff',fg="#6d6d6d",anchor=W).pack(pady=5,padx=10,fill=X)
        for interet in profil.interets:
            Label(self.userInsideFrame,font="GENERALFONT 9",text=interet,bg='#ffffff',fg="#6d6d6d",anchor=W).pack(pady=1,padx=20,fill=X)
            
        Label(self.userInsideFrame,height=0,bg="#ffffff").pack(pady=0,fill=X)
        
        self.userSearch.insert(1.0,profil.email)
        
    def deleteUser(self):
        self.userPanelContent.destroy()
    
    def updateView(self,publications,users,resetPosition=1):
        self.printY = 0
        self.scrollY = 0
        self.centerCanvevas.delete("post")
        
        for post in publications:
            self.printPost(post,users[post.posterID])
            if(resetPosition):
                self.centerCanvevas.config(scrollregion=(0, 0, 0, self.scrollY+10))
        self.centerCanvevas.config(scrollregion=(0, 0, 0, self.scrollY+10))
            
    def refreshView(self,publications,users,resetUser=1,resetPosition=1):
        self.centerCanvevas.delete("post")
        self.scrollY = 0
        self.printY = 0
        for post in publications:
            self.printPost(post,users[post.posterID])
            if(resetPosition):
                self.centerCanvevas.config(scrollregion=(0, 0, 0, self.scrollY+10))
                
        if resetUser == 1:
            self.deleteUser()
            self.userSearch.delete(1.0, END)
            self.userSearch.insert(1.0,"None")
        
    def printPost(self,post,poster,border="#e0e0e0"):
        name = poster.prenom+" "+poster.nom
        message = post.message
        profilId = poster.id
        id = post.id
        date = post.date
        follow = poster.follow
        
        # Setting de la date
        strDate=""
        maintenant = datetime.now()
        
        if date.date() == maintenant.date() and maintenant.hour - date.hour < 1 :
            if maintenant.minute - date.minute > 0:
                strDate += str(maintenant.minute-date.minute)+"min"
            else:
                strDate += "<1min"
        else:
            if date.date() != maintenant.date():
                strDate += str(date.year)+"-"+str(date.month)+"-"+str(date.day)+" | "
            
            strDate += str(date.hour)+"h "
                
            if date.minute > 9:
                strDate += str(date.minute)
            else:
                strDate += "0"+str(date.minute)
        
        # Setting de la page
        color="#ffffff"
        left = 30
        top = self.printY+30
        right = CANVAS_DIM_X-30
        padding = 10
        
        width = right-padding*3-left
        font="GENERALFONT 8"
        
        # texte sur canevas
        publication = self.centerCanvevas.create_text(left+padding*2,top+padding*3,text=message,anchor=NW,width=width,tags=("post","text",))
        
        bounds = self.centerCanvevas.bbox(publication)
        height = (bounds[3] - bounds[1])+30

        bott = top+height+20
        
        # texte dans window sur canevas
        text = Text(self.centerCanvevas,fg="grey25",bd=0,font=font,selectbackground="#e57360")
        text.pack()
        text.insert(END,message)
        text.config(state=DISABLED)
        
        window = self.centerCanvevas.create_window(left+padding*2,top+padding*3,window=text,anchor=NW,width=width,height=height-30,tags=("post","window",))
        
        # Rectangle de publication
        self.centerCanvevas.create_rectangle(left,top,right,bott,fill=color,width=0,tags=("post","bg",))
        
        # Coins arrondis
        self.centerCanvevas.create_oval(right-padding,top-padding,right+padding,top+padding,fill=color,width=0,tags=("post","coin",))
        self.centerCanvevas.create_oval(left-padding,top-padding,left+padding,top+padding,fill=color,width=0,tags=("post","coin",))
        self.centerCanvevas.create_oval(right-padding,bott-padding,right+padding,bott+padding,fill=color,width=0,tags=("post","coin",))
        self.centerCanvevas.create_oval(left-padding,bott-padding,left+padding,bott+padding,fill=color,width=0,tags=("post","coin",))
        
        # Bordure 
        self.centerCanvevas.create_rectangle(left,top-padding,right,top,fill=color,width=0,tags=("post","border",))
        self.centerCanvevas.create_rectangle(left,bott,right,bott+padding+1,fill=color,width=0,tags=("post","border",))
        self.centerCanvevas.create_rectangle(left-padding,top,left,bott,fill=color,width=0,tags=("post","border",))
        self.centerCanvevas.create_rectangle(right,top,right+padding+1,bott,fill=color,width=0,tags=("post","border",))
        
        self.centerCanvevas.create_line(left+padding*1,top+padding*1.5,right,top+padding*1.5,width=2,fill=border,tags=("post","element",))
        self.centerCanvevas.create_oval(left-padding*2,top-padding*2,left+padding*2,top+padding*2,fill=color,outline=border,width=2,tags=("post","element",))
        
        # ID bubble
        font="GENERALFONT 12 bold"
        self.centerCanvevas.create_text(left,top,text=id, font = font, fill=COLOR_TXT,tags=("post","postID",))
        
        # Profil name
        font="GENERALFONT 10 bold"
        
        if(poster.id == self.parent.MyID):
            self.centerCanvevas.create_text(left+padding*3,top+padding/3,text=name,anchor=W,font = font, fill=COLOR_RED,tags=("post",))
        else:
            if follow:
                color = "#f9b450"
            else:
                color = COLOR_TXT
            self._userDic[self.centerCanvevas.create_text(left+padding*3,top+padding/3,text=name,anchor=W,font = font,activefill=COLOR_TXTACTIVE, fill=color,tags=("post","username",))] = profilId
        
        mydict = self.parent.modele.interets
        sujet = list(mydict.keys())[list(mydict.values()).index(post.sujet)]
        self.centerCanvevas.create_text((right-left)/2,top+padding/3,text=sujet,font = font, fill=COLOR_TXT,tags=("post",))
        
        # Date
        font="GENERALFONT 8"
        self.centerCanvevas.create_text(right-padding,top+padding/3,text=strDate,anchor=E, font = font, fill="#999999",tags=("post","date",))
        
        #self.centerCanvevas.tag_raise(post)
        self.centerCanvevas.delete("text")
        
        self.scrollY = self.scrollY+height+70
        self.printY = self.printY+height+70
        
        
    def OnMouseWheel(self,event):
        count = 0
        if event.num == 5 or event.delta == -120:
            count =1
        if event.num == 4 or event.delta == 120:
            count =-1
        self.centerCanvevas.yview('scroll', count, 'units')
        self.centerCanvevas.update()
        
    def mousePress(self,event):
        x, y = event.x, self.centerCanvevas.canvasy(event.y)
        item=self.centerCanvevas.find_closest(x, y, 2)[0]
        if "username" in self.centerCanvevas.gettags(item):
            profilID = self._userDic[item]
            self.userSearch.delete("1.0", END)
            
            self.parent.showUser(profilID)
        else:
            self.deleteUser()
            self.userSearch.delete(1.0, END)
            self.userSearch.insert(1.0,"None")
            
    def searchOption(self):
        sujet = self.tkvar.get()
        email = self.userSearch.get(1.0, 'end-1c')
        self.parent.passSearch(sujet,email)
        pass
            

    def windowPublishPost(self,interets):
        self.publishPause = Toplevel(height=500, width=700)
        self.publishPause.title("Publier")
        self.publishPause.configure(background = 'grey25')
        
        self.tkpost.set(list(interets.keys())[0])

        """
        w = self.publishPause.winfo_screenwidth()
        h = self.publishPause.winfo_screenheight()
        size = tuple(int(_) for _ in self.publishPause.geometry().split('+')[0].split('x'))
        x = w/2 - size[0]/2
        y = h/2 - size[1]/2"""
        
        self.publishpostFrame = Frame(self.publishPause)
        self.publishpostFrame.configure(pady = 15, padx = 30, background = "#e05841")
        self.publishpostFrame.pack()
        
        self.textePublication = Text(self.publishpostFrame, height = 5, width = 55)
        self.textePublication.pack()
        
        self.boutonPostFrame = Frame(self.publishpostFrame)
        self.boutonPostFrame.configure(pady = 20, background =  "#e05841")
        self.boutonPostFrame.pack()
        
        label = Label(self.boutonPostFrame, text="Type de publication : ", font = "GENERALFONT 12 bold", fg = 'white', bg="#e05841")
        label.grid(padx = 15, column = 0, row = 0)
        
        sujetMenu = OptionMenu(self.boutonPostFrame, self.tkpost, *interets)
        sujetMenu.configure(font = "GENERALFONT 12 bold",relief=FLAT,bd = 0, fg=COLOR_TXT,activeforeground=COLOR_TXTACTIVE,bg="#ffffff",activebackground="#ffffff")
        sujetMenu.grid(column = 1, row = 0)
        
        b = Button(self.boutonPostFrame, text="Publier", font = "GENERALFONT 14", anchor = E, command = self.creationPost)
        #b.config(width=1) 
        b.grid(padx = 25, column = 2, row = 0)
        
        #Appel pour centrer la fenÃªtre
        self.center(self.publishPause)
        
    def creationPost(self):
        subject = self.tkpost.get()
        txt = self.textePublication.get(1.0, 'end-1c')      
        self.parent.creationPost(subject,txt)
        self.updatePourPost()
            
    def updatePourPost(self):
        self.publishPause.destroy()
        self.parent.refreshView()
        
    def center(self,win):
        """
        centers a tkinter window
        :param win: the root or Toplevel window to center
        """
        win.update_idletasks()
        width = win.winfo_width()
        frm_width = win.winfo_rootx() - win.winfo_x()
        win_width = width + 2 * frm_width
        height = win.winfo_height()
        titlebar_height = win.winfo_rooty() - win.winfo_y()
        win_height = height + titlebar_height + frm_width
        x = win.winfo_screenwidth() // 2 - win_width // 2
        y = win.winfo_screenheight() // 2 - win_height // 2
        win.geometry('{}x{}+{}+{}'.format(width, height, x, y))
        win.deiconify()
    
    def changeFollow(self,txt):
        self.btnFollow.config(text=txt)