# -*- coding: utf-8 -*-

import sqlite3
from _datetime import datetime
from _overlapped import NULL

class BD():
    
    def __init__(self,GENERATECONTENT=False):
        self.conn = sqlite3.connect('example.db')
        self.GENERATECONTENT = GENERATECONTENT
        self.creation_bd()
        
        
        
    def creation_bd(self):
        c = self.conn.cursor()
        
        if self.GENERATECONTENT:
            self.dropTables()
        
        #Creation Table expertises
        c.execute('''CREATE TABLE if not exists expertises 
                     (id integer not null primary key,
                      nom text) ''')
        
        ##Creation table usagers
        c.execute('''CREATE TABLE if not exists users 
                     (id integer not null primary key AUTOINCREMENT,
                      nom text,
                      prenom text,
                      password text,
                      status text,
                      email text,
                      icon blob,
                      abonnement text,
                      nombre_de_credit integer,
                      connected integer,
                      moduleDispo integer,
                      id_expertise integer,
                      FOREIGN KEY(moduleDispo) REFERENCES user_module(id),
                      FOREIGN KEY(id_expertise) REFERENCES expertises(id)) ''')
        
        
        ##Creation table follow
        c.execute('''CREATE TABLE if not exists follow 
                     (id integer not null primary key AUTOINCREMENT,
                      id_user integer not null,
                      followed_id integer not null)''')
        
        ##Creation de la table publication
        c.execute('''CREATE TABLE if not exists publication 
                     (id integer not null primary key AUTOINCREMENT,
                      message text,
                      publishdate text,
                      rank integer,
                      userID integer,
                      interetID integer,
                      FOREIGN KEY (userID) REFERENCES users(id),
                      FOREIGN KEY (interetID) REFERENCES interet(id))''')
        
        ##Creation de la table interets
        c.execute('''CREATE TABLE if not exists interet 
                     (id integer not null primary key AUTOINCREMENT,
                      nom text)''')
        
        ##Creation de la table d'association user_interets
        c.execute('''CREATE TABLE if not exists user_interet 
                     (user_id integer not null,
                      interet_id integer not null,
                      PRIMARY KEY(user_id,interet_id),
                      FOREIGN KEY (user_id) REFERENCES users(id),
                      FOREIGN KEY(interet_id) REFERENCES interet(id)) ''')
        
        
        ##Creation de la table portfolio
        c.execute('''CREATE TABLE if not exists porfolio 
                     (id integer not null primary key AUTOINCREMENT,
                      titre text,
                      expertise_en_lien text,
                      description text,
                      image blob,
                      FOREIGN KEY(id) REFERENCES expertises(id),
                      FOREIGN KEY(id) REFERENCES users(id)) ''')
        
        #Creation Table CV
        c.execute('''CREATE TABLE if not exists cv 
                     (id integer not null primary key,
                      nom text,
                      prenom text,
                      email text,
                      tel text) ''')
        
        #Creation Table Message
        c.execute('''CREATE TABLE if not exists messages 
                    (id integer not null primary key,
                     message text,
                     date text) ''')
        
        #Creation Table Scolarite
        c.execute('''CREATE TABLE if not exists scolarite 
                    (id integer not null primary key,
                     nom_ecole text,
                     titre_etude text) ''')
        
        
        #Table d'association cv_scolarite
        c.execute('''CREATE TABLE if not exists cv_scolarite 
                    (cv_id integer not null,
                     scolarite_id integer not null,
                     PRIMARY KEY (cv_id, scolarite_id),
                     FOREIGN KEY(cv_id) REFERENCES cv(id),
                     FOREIGN KEY(scolarite_id) REFERENCES scolarite(id) )''')
        
        #Table d'association messages_users
        c.execute('''CREATE TABLE if not exists messages_users 
                    (messages_id integer not null,
                     poster_id integer not null, 
                     receiver_id integer not null, 
                     PRIMARY KEY (messages_id, poster_id, receiver_id),
                     FOREIGN KEY(messages_id) REFERENCES messages(id),
                     FOREIGN KEY(poster_id) REFERENCES users(id), 
                     FOREIGN KEY(receiver_id) REFERENCES users(id) )''')
        
        #Table module
        c.execute('''CREATE TABLE if not exists module 
                    (id integer not null primary key,
                    nom text,
                    version integer
                 )''')

        
        ##Creation table module
        c.execute('''CREATE TABLE if not exists user_module 
                    (id integer not null primary key AUTOINCREMENT,
                     user_id integer not null, 
                     module_id integer not null,
                     FOREIGN KEY(user_id) REFERENCES users(id),
                     FOREIGN KEY(module_id) REFERENCES module(id))''')
        
        self.conn.commit()
        
    def dropTables(self):
        c = self.conn.cursor()
        c.execute('DROP TABLE IF EXISTS users')
        c.execute('DROP TABLE IF EXISTS interet')
        c.execute('DROP TABLE IF EXISTS user_interet')
        c.execute('DROP TABLE IF EXISTS publication')
        c.execute('DROP TABLE IF EXISTS follow')
        c.execute('DROP TABLE IF EXISTS expertises')
        
        self.conn.commit()
        
     
    def getUser(self, id):
        c = self.conn.cursor() 
        c.execute('SELECT id, prenom,nom, email,(SELECT nom FROM expertises WHERE id = id_expertise) FROM users  WHERE id = ?',(id,))
        result = c.fetchone()
        return result
    
    def insertUser(self,prenom,nom,password,email,expertise):
        c = self.conn.cursor()
        id = c.execute('''INSERT INTO users(nom,prenom,password,status,email,icon,abonnement,nombre_de_credit,connected,id_expertise) 
                     VALUES(?,?,?,?,?,?,?,?,?,?)''',(nom,prenom,password,'Travailleur',email,NULL,'Non','0','0',expertise,))
        
        self.conn.commit()             
        return id       
    
    def connectUser(self,id):
        c = self.conn.cursor()
        c.execute('UPDATE users SET connected = 1 WHERE id = ',(id,))
        self.conn.commit() 
        
    def disconnectUser(self,id):
        c = self.conn.cursor()
        c.execute('UPDATE users SET connected = 0 WHERE id = ',(id,))
        self.conn.commit()  
    
    def newGetPublications(self,postID=-1,qty=10,sujet="None",email="None",userID=None):
        c = self.conn.cursor()
        query = '''SELECT id,message,userID,publishdate,rank,interetID 
        FROM publication
        WHERE id > 0'''
        
        if postID != -1:
            query += ' AND id < '+str(postID)
            
        if sujet == "None" and email == "None":
            if userID:
                interets = self.getInteretUser(userID)
                if len(interets) > 0:
                    query += ' AND (interetID IN (SELECT interet_id FROM user_interet WHERE user_id = '+str(userID)+')'
                    query += ' OR userID IN (SELECT followed_id FROM follow WHERE id_user = '+str(userID)+'))'
        else:
            if sujet != "None":
                query += ' AND interetID IN (SELECT id FROM interet WHERE nom = "'+str(sujet)+ '")'
            
            
            if email != "None":
                check = self.getUserByEmail(email)[0]
                
                if check:
                    query += ' AND userID = '+str(check)+ ' '
        
        
        query += ' ORDER BY id DESC LIMIT '+str(qty)
        
        c.execute(query)   
        result = []
        for element in c:
            #print(element[0])
            result.append(element)
        return result 
    
    def getUserByEmail(self,email):
        c = self.conn.cursor()
        c.execute('''SELECT id FROM users WHERE email = ? ''', (email,))
        result = c.fetchone()
        
        return result
    
    def insertPublication(self, message, userID, interetID):
        c = self.conn.cursor()
        c.execute('''INSERT INTO publication(message,userID,interetID,publishdate,rank)
                     VALUES(?,?,?,?,?)''',(message, int(userID),int(interetID),datetime.now(),1,))
        self.conn.commit()
        return 0    
    
    def getExpertiseName(self, idExpertise):
        c = self.conn.cursor()
        c.execute('''SELECT nom FROM expertises WHERE id = ? ''', (idExpertise,))
        result = c.fetchone()
        return result
    
    def getExpertises(self):
        c = self.conn.cursor()
        c.execute('SELECT id,nom from expertises')   
        result = []
        for element in c:
            result.append(element)
        return result 
        
        
    def insertExpertise(self, nom):
        c = self.conn.cursor()
        c.execute('INSERT INTO expertises(nom) VALUES(?)',(nom,))
        self.conn.commit()
        
    #Mettre le user online car il ne l'est pas lors de son insertion
    def putUserOnline(self, id):
        c = self.conn.cursor()
        c.execute('''UPDATE users SET connected = 1 WHERE id = ? ''', (id,))
        
    
    
    def getIdentifiers(self, email, password):
        c = self.conn.cursor()
        c.execute('SELECT * FROM users WHERE email = "'+ email +'" AND password = "'+ password +'" ')    
        check = c.fetchone()
        return check       
         
    
    def getUserTest(self):
        c = self.conn.cursor()
        c.execute('SELECT * FROM users')    
        check = c.fetchone()
        return check       
    
    def insertModule(self, moduleName, version):
        c = self.conn.cursor()
        c.execute('''INSERT INTO module(nom,version) 
                     VALUES(?,?)''',(moduleName, version,))
        self.conn.commit()
        
    def dropModules(self):
        c = self.conn.cursor()
        c.execute('DELETE FROM module')
    
    def checkIfModuleExist(self, moduleName):
        c = self.conn.cursor()
        c.execute('''SELECT * from module where nom = ?''', (moduleName,))
        result = []
        for element in c:
            result.append(element)
        return result 

    def userConnected(self, email):
        c = self.conn.cursor()
        c.execute(' UPDATE users SET connected = 1 WHERE email = "'+ email +'" ')
        c.execute(' SELECT id FROM users WHERE email = "'+ email +'" ')
        val = c.fetchone()
        return val
    
    def getMessagesEnvoye(self,idUser):
        c = self.conn.cursor()
        idMessage = c.execute('''SELECT messages_id FROM messages_users WHERE poster_id = ?''', idUser)
        result = []
        for element in idMessage:       
            c.execute('''SELECT message FROM messages WHERE id = ? ORDER BY date DESC''', element)
            result.append(element)
        return result
    
    def getMessagesRecu(self, idUser):
        c = self.conn.cursor()
        idMessage = c.execute('''SELECT messages_id FROM messages_users WHERE receiver_id = ? ''', idUser)
        result = []
        for element in idMessage:
            c.execute('''SELECT message FROm messages WHERE id = ? ORDER BY date DESC ''', element)
            result.append(element)
        return result
        
    def insertMessage(self,idUser,idReceiver, message):
        c = self.conn.cursor()
        c.execute('''INSERT INTO messages(message,date) VALUES(?,?)''',message,datetime.now())
        idMessage = c.lastrowid
        c.execute('''INSERT INTO messages_users VALUES(?,?,?)''', idMessage, idUser, idReceiver)
        self.conn.commit()   
            
    def closeDB(self):
        self.conn.close()
        
    #************************************************************ Method Alex **************************************************
    def insertInteret(self, interet):
        c = self.conn.cursor()
        c.execute('INSERT INTO interet(nom) VALUES(?)',(interet,))
        self.conn.commit()
        
    def getInterets(self):
        c = self.conn.cursor()
        c.execute('SELECT id,nom from interet')   
        result = []
        for element in c:
            result.append(element)
        return result 
    
    def getInteretsID(self):
        c = self.conn.cursor()
        c.execute('SELECT id from interet')   
        result = []
        for element in c:
            result.append(element)
        return result 
    
    def getUsers(self):
        c = self.conn.cursor()
        c.execute('SELECT id from users')   
        result = []
        for element in c:
            result.append(element)
        return result 
    
    def getLastUserID(self):
        c = self.conn.cursor()
        c.execute('SELECT id FROM users ORDER BY id DESC LIMIT 1')    
        check = c.fetchone()
        return check
    
    def getInteretUser(self,id):
        c = self.conn.cursor()
        c.execute('SELECT nom FROM interet WHERE id IN (SELECT interet_id FROM user_interet WHERE user_id = ?)',(id,))   
        result = []
        for element in c:
            result.append(element)
        return result 
    
    def getInteretID(self,id):
        c = self.conn.cursor()
        c.execute('SELECT nom FROM interet WHERE id = ?)',(id,))   
        check = c.fetchone()
        return check 
    
    def linkUserInteret(self,userId,interetID):
        c = self.conn.cursor()
        c.execute('INSERT INTO user_interet(user_id, interet_id) VALUES(?,?)',(userId,interetID))
        self.conn.commit()
        
    def getFollowsByID(self,id):
        c = self.conn.cursor()
        c.execute('SELECT followed_id FROM follow WHERE id_user = ?',(id,))   
        result = []
        for element in c:
            result.append(element)
        return result 
    
    def follow(self,followerID,followedID):
        c = self.conn.cursor()
        c.execute('INSERT INTO follow(id_user,followed_id) VALUES(?,?)',(followerID,followedID,))
        self.conn.commit()
        return "Follow"
    
    def unfollow(self,followerID,followedID):
        c = self.conn.cursor()
        c.execute('DELETE FROM follow WHERE id_user = ? AND followed_id = ?',(followerID,followedID,))
        self.conn.commit()
        return "Unfollow"
    
    
    
if __name__=="__main__":
    c=BD()
    print("End FaceMaster")