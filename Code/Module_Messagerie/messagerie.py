# -*- coding: utf-8 -*-
# Version 
# 1

import xmlrpc.client
import os,os.path
import sys
import socket
from subprocess import Popen 

from SETTINGS import *
from Vue import *
from Modele import * 


class Controleur():
    def __init__(self,id=1):
        self.modele= Modele(self) 
        self.vue= Vue(self)
        self.server=None
        self.MyID = id
        self.sociableUserId = []
        
        #*******************WHY?**********************#
        self.actualConvo = 0
        
        self.loginclient()
        
        # Ne faire la suite que si un server est connectÃ©
        if self.server:
            self.iniModule()
            #self.refreshMessages() # Ça sert à quoi, déjà prévue dans checkNew, voir commentaire laissé là
            self.vue.root.mainloop()
            #self.getRandomMessage() # On a déjà des convo générer, pourquoi en générer d'autres?



        
    def loginclient(self,ip="10.57.251.57",port="50017"):
        ad="http://"+ip+":"+port
        self.server=xmlrpc.client.ServerProxy(ad)
        print(self.server)    
        
    def getUserConvos(self):
        usersID = self.server.getUsersMessage(self.MyID)
        for user in usersID:
            userID = -1
            if user[0] != self.MyID:
                if user[0] not in self.sociableUserId:
                    self.sociableUserId.append(user[0])
                userID = user[0]
            else:
                if user[1] not in self.sociableUserId:
                    self.sociableUserId.append(user[1])
                userID = user[1]
            
            user = self.server.getUser(userID)
            self.modele.addProfil(user)
        
         
    def getFirstConvo(self,userID):
        messagesID = self.server.getConvo(self.MyID,userID)
        for messageID in messagesID:
            message = self.server.getMessageByID(messageID[0])
            
            mine = False
            if messageID[1] == self.MyID:
                mine = True
                
            self.modele.addMessage(messageID,message, userID, mine)
            
    def getNextMessages(self):
        print(self.modele.conversations[self.actualConvo].messages[-1].id)
        print(self.modele.conversations[self.actualConvo].messages[-1])
        
        messagesID = self.server.getNextMessages(self.MyID,self.actualConvo,self.modele.conversations[self.actualConvo].messages[-1].id)
        for messageID in messagesID:
            message = self.server.getMessageByID(messageID[0])
            
            mine = False
            if messageID[1] == self.MyID:
                mine = True
                
            self.modele.addMessage(messageID,message, self.actualConvo, mine)
    
    def iniModule(self):
        self.getUserConvos()
        self.actualConvo = list(self.modele.users.keys())[0]
        self.getFirstConvo(self.actualConvo)
        self.vue.iniModule(self.modele.conversations,self.modele.users,self.actualConvo)
        self.checkNew()    
        
    def checkNew(self):
        if self.vue.centerCanvevas.yview()[0] < 0.1 and self.vue.centerCanvevas.yview()[1] < 0.9:
            
            #self.modele.genRandomConvo(10) # Cette méthode n'existe plus, pourquoi appeler un genRandom quand on peut juste appeler d'autres messages du server. Voir vitrine sur comment faire
            self.getNextMessages()
            
            self.vue.updateCanevas(self.modele.conversations[self.actualConvo],self.modele.users[self.actualConvo],0)
        elif self.vue.centerCanvevas.yview()[1] > 0.9:
            # Regarder si le dernier message local de cette convo est le dernier message server
            # Si non, refresh la page
            pass
        self.vue.root.after(1000,self.checkNew)
        
    def changeConvo(self,userID):
        self.actualConvo = userID
        self.refreshView()
        
    def refreshView(self,resetPosition=1):
        self.modele.clearConvos()
        self.getUserConvos()
        self.getFirstConvo(self.actualConvo)
        
        self.vue.updateCanevas(self.modele.conversations[self.actualConvo],self.modele.users[self.actualConvo],resetPosition)

    def addMessage(self,idUser,idReceiver, message):
        self.server.addMessage(idUser,idReceiver, message)

    def filterUserConvo(self, userNameEntry):
        userNameList = []
        searchResult = []
        print(self.sociableUserId)
        for id in self.sociableUserId:
            val = self.server.getUserFullName(id)
            userNameList.append(val)
        userNameEntry = userNameEntry.split();
        for index, userName in enumerate(userNameList):
            for entry in userNameEntry:
                if entry == userName[0][0] or entry == userName[0][1]:
                    searchResult.append(self.sociableUserId[index])     
        usersToUpdate = {}

        
        for resultId in searchResult:
            usersToUpdate[resultId] = self.modele.users[resultId]
        

        if len(userNameEntry) == 0 and len(usersToUpdate.keys()) == 0:
            self.vue.updateUserAfficheAGauche(self.modele.users)
        else:
            self.vue.updateUserAfficheAGauche(usersToUpdate)

    def getRandomMessage(self):
        message = self.server.getRandomMessage
        #faire afficher message avec le message reçu
        
    def refreshMessages(self):
        print("refresh messages")
        self.refreshView()
        self.vue.root.after(1000, self.refreshMessages)

if __name__ == '__main__':
    c=Controleur()