from SETTINGS import *
from datetime import date, time, datetime

class Message():    
    def __init__(self,id=-1,message="",mine=False,date=str(datetime.now())):
        self.id = id
        self.message = message
        self.date = datetime.strptime(date,'%Y-%m-%d %H:%M:%S.%f')

        self.mine = mine
    