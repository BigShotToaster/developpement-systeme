from SETTINGS import *
import Message
    
class Conversation():    
    def __init__(self,userId):
        self.userId = userId
        self.messages = []
    
    def addMessage(self, message):
        self.messages.append(Message(message[0],message[1],message[2])) 

    def clearMessage(self):
        self.messages.clear()