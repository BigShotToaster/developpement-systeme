from SETTINGS import *
    
class User():    
    def __init__(self,parent,id,email="test@email.com",nom="Smith",prenom="John",sujets=[""]):
        self.parent = parent
        self.id = id
        self.nom = nom
        self.prenom = prenom
        self.email = email
        self.interets = sujets
        self.status = "Travailleur"
        self.expertise = "Informatique"
        
        self.follow = False