from SETTINGS import *
from datetime import date, time, datetime
    
class Publication():    
    def __init__(self,parent,id,poster,message,sujet,date=None,rank=0):
        self.parent = parent
        self.id = id
        self.posterID = poster
        self.message = message
        self.date = datetime.strptime(date,'%Y-%m-%d %H:%M:%S.%f')
        self.rank = rank
        self.sujet = sujet
        